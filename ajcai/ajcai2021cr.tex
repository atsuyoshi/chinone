% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}

%
\usepackage{graphicx}

\usepackage{algorithm}
\usepackage[noend]{algorithmic}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{amsmath}

\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}
\renewcommand{\textfraction}{0.0}
\def\ci#1{\scriptstyle  (\pm #1)}

% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}

\begin{document}
%
\title{An Explainable Recommendation Based on Acyclic Paths in an Edge-Colored Graph}
%
\titlerunning{An Explainable Rec. Based on Acyclic Paths in an Edge-Colored Graph}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here

%
\author{Kosuke Chinone \and
Atsuyoshi Nakamura\orcidID{0000-0001-7078-8655}}
%
\authorrunning{K. Chinone and A. Nakamura}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Graduate School of Information Science and Technology, Hokkaido University\\
\email{\{chinone,atsu\}@ist.hokudai.ac.jp}}
%
\maketitle              % typeset the header of the contribution
%
\begin{abstract}
We propose a novel recommendation algorithm based on acyclic paths in an edge-colored graph. In our method, all the objects including users, items to recommend, and other things usable to recommendation are represented as vertices in an edge-colored directed graph, in which edge color represents relation between the objects of its both ends. By setting each edge weight appropriately so as to reflect how much the object corresponding to its one end is preferred by people who prefer the object corresponding to its other end,  the probability of an $s$-$t$ path, which is defined as the product of its component edges' weights, can be regarded as preference degree of item $t$ (item corresponding to vertex $t$) by user $s$ (user corresponding to vertex $s$) in the context represented by the path. Given probability threshold $\theta$, the proposed algorithm recommends user $s$ to item $t$ that has high sum of the  probabilities of all the acyclic $s$-$t$ paths whose probability is at least $\theta$. For item $t$ recommended to user $s$, the algorithm also shows high probability color sequences of those $s$-$t$ paths, from which we can know main contexts of the recommendation of item $t$ for user $s$. 
According to our experiments using real-world datasets, the recommendation performance of our method is comparable to the non-explainable state-of-the-art recommendation methods.
\keywords{recommender system \and explainablity \and graph algorithm}
\end{abstract}
%
%
%
\section{Introduction}
Now, recommender systems are used in many web services to present items suitable for user's preference, 
and explainability is perceived as one of the points to be improved in them \cite{zhang2018explainable}.
In addition to recommendation, explainable systems can also provide recommendation reasons. 
Explainability improves the transparency, effectiveness, trustworthiness, satisfaction, and persuasiveness of recommender systems. 
The classical recommendation method of user or item similarity-based collaborative filtering \cite{aggarwal1999horting,sarwar2001item} can be seen as implicitly providing explanations like ``the item was purchased by users whose purchase histories are similar to that of yours'' or ``the item was purchased by a group of users who purchased the items you bought''.
Recent recommendation models with high preference-prediction accuracy, 
such as models that learn latent factors using matrix factorization to predict items' ratings by users \cite{koren2008factorization}, and deep learning models \cite{wang2015collaborative}, however, are difficult to understand the reason for recommendation intuitively.

On the other hand, there are efforts to use various relations between users, items and other related objects, to improve the accuracy and interpretability of recommendations \cite{MRH}.
Such relations between objects can be represented by a graph, in which objects are corresponding to vertices and 
relations between objects are represented by edges between vertices of corresponding objects.
Considering preference-propagation degree based on each relation in the graph, items can be recommended to users by calculating
estimated preference degree of items for the users.
One merit of this graph-based method is that the paths most contributing to the estimated preference degree can be shown as 
explanation for the recommendation.
In previous work, such preference degree estimation is done by calculating the stationary distribution of the Markov process
with transition probability corresponding to preference-propagation degree.
The approximate calculation of this method is efficiently done by repeated matrix multiplication,
but contributed paths for items with high estimated preference degree can not be obtained as by-product.


In this paper, we propose a novel recommendation algorithm based on acyclic paths in an edge-colored directed graph constructed from various relations between recommendation-related objects. 
In an edge-colored directed graph, an edge color represents a relation between the objects that correspond to the vertices of its both ends.
Given appropriate edge propagation probabilities and threshold $\theta$, our algorithm calculates the probability sum of all the acyclic $s$-$t$ paths whose probability is at least $\theta$. On the way of the calculation, our algorithm also calculates the probability sum of each color sequence of those $s$-$t$ paths.
Then, high probability items $t$ are recommended to user $s$ accompanied by high probability color sequences of the $s$-$t$ paths, from which we can know main contexts of the recommendation of item $t$ for user $s$.
According to our experimental results using real-world datasets, the recommendation performance of our proposed method is comparable to the non-explainable state-of-the-art recommendation methods.
By checking recommendation list with the highest probability color sequences, color sequences are confirmed to be suitable for explanation of the recommendation. Furthermore, different trends are observed between the lists with different color sequences,
which indicates the possibility of using color sequences to provide the recommendation lists of different trends.

\subsection*{Related Work}

Though the term ``explainable recommendation system'' has only recently begun to be used \cite{zhang2014explicit},
such recommender systems had existed before then.
User-based or item-based collaborative filtering \cite{aggarwal1999horting,herlocker2000explaining,sarwar2001item} can be seen as a kind of explainable recommender system because they recommend items based on user-similarity or item-similarity of ratings, and such similarities can be seen as explanation.

Explicit Factor Model (EFM) \cite{chen2016learning,zhang2015daily} is an explainable recommendation method
that is made from Latent Factor Model (LFN) \cite{koren2008factorization} by modifying it so as to have explainability.
Some of recent recommendation systems using deep neural networks such as CNN \cite{seo2017interpretable} and RNN \cite{donkers2017sequential} are working on automatic generation of explanations by natural language generation models \cite{li2017neural}. 
It is said to be a difficult but important problem to explain the recommendation produced by latent factor and deep learning models \cite{zhang2018explainable}.

Recent explainable recommendation systems use social information such as purchase histories, review information, and friendships between users and items \cite{papadimitriou2012generalized,MRH}. 
There is one that uses tripartite graphs to represent the relationship between users, items, and aspects \cite{he2015trirank}, where an aspect is a feature of an item extracted from a user's review.
Knowledge graphs, which contain a wealth of information about users and items, have been used to generate intuitive and more suitable explanations for recommended items \cite{catherine2017explainable}. 


\section{Preliminary}
An \emph{edge-weighted edge-colored directed graph} $G$ is a triplet $G(V,E,p)$, where $V$ is a set of vertices, $E\subseteq V\times V\times C$ is a set of directed colored edges with a color set $C$,
and  $p$ is an edge weight function $p:E\rightarrow (0,\infty)$.
Note that multiple edges $(u,v,c_1), (u,v,c_2)\in V\times V\times C$ are allowed if their colors $c_1, c_2$ are different.
As for an edge weight function, we only consider the function $p$ satisfying the probability condition that $\sum_{(v,c):(u,v,c)\in E}p((u,v,c)) =1$ for all $u\in V$.
An \emph{acyclic $s$-$t$ path} in $G$ is defined as an edge sequence $(s,v_1,c_1)(v_1,v_2,c_2)\cdots (v_{k-1},v_k,c_k)(v_k,t,c_{k+1})$ for some $k\leq |V|$ in which
$s, v_1, v_2, \dots, v_k,t$ must be different from each other. For an $s$-$t$ path $(s,v_1,c_1)(v_1,v_2,c_2)\cdots (v_{k-1},v_k,c_k)(v_k,t,c_{k+1})$, we call $c_1c_2\cdots c_{k+1}$ the \emph{color sequence} of the $s$-$t$ path, and define the \emph{probability} of the $s$-$t$ path as $p((s,v_1,c_1))\times p((v_1,v_2,c_2)) \times\cdots\times p((v_{k-1},v_k,c_k))\times p((v_k,t,c_{k+1}))$ .

\section{Proposed Method}

In this section, we propose an explainable recommendation model based on acyclic paths in an edge-colored graph.
First, we represent the information used for recommendation as an edge-colored graph, and introduce our recommendation method based on acyclic paths. Then, we describe our recommendation explanation by color sequences.

\subsection{Graph Construction}

We construct an edge-weighted edge-colored directed graph $G(V,E,p)$ for recommendation using various information such as rating, tag, item attribute and so on.
Let $V$ be the set of all the objects that appear in the information used in recommendation, that is, $V$ is composed of users, items, tags, attributes, etc.
Each directed colored edge $(u,v,c)$ is created if object $u$ has a certain relation (that is represented by color $c$) with object $v$.
For example, use color $\mathbf{R}$ for rating relation. Then, edge $(u,v,\mathbf{R})$ is created if user $u$ rated item $v$.
We are dealing with a directed graph, so we use different color for reverse relation.
Thus, in the case that \textit{rated} relation (that is represented by color $\mathbf{r}$) is considered in addition to rating relation, edge $(v,u,\mathbf{r})$ is also created.
There is a case that different colored edges from $u$ to $v$ are created.
Assume that color $\mathbf{T}$ is used for tagging relation, edge $(u,v,\mathbf{T})$ is created if user $u$ tagged item $v$ with some tag,
which is independently done from the creation of edge $(u,v,\mathbf{R})$ for rating relation.
An edge weight function $p((u,v,c))$ is set to the value that represents how much object $v$ is preferred if object $u$ is preferred.
Such preference propagation degree depends on the relation corresponding to the color $c$ of the edge.
So, first, we construct a function $w_c:E\cap(V\times V\times \{c\})\rightarrow (0,\infty)$ for each color $c$ depending on its representing relation.
Then, an edge weight function $p$ is constructed by normalizing $w_c$ for all colors $c$.

\subsection{Acyclic-Path-Based Recommendation}
We propose a recommendation method that recommends an item $t$ to a user $s$ based on acyclic $s$-$t$ paths on an edge-colored directed graph $G(V,E,p)$.
For each user $s$, the sum $P_t$ of the probabilities of all the acyclic $s$-$t$ paths in $G$  is calculated for each item $t$,
then items $t$ with the largest $P_t$ are recommended.
One merit of this acyclic path-based recommendation, we can show high probability path information as explanation for the recommendation.
As such information, we use the color sequences of high probability acyclic $s$-$t$ paths, which are relation sequences because each color is corresponding to different relation. For each item $t$, the sum $P_t[\mathbf{c}]$ of the probabilities of all the acyclic $s$-$t$ paths in $G$ with color sequence $\mathbf{c}$ is calculated for each appearing color sequence $\mathbf{c}$, then color sequences $\mathbf{c}$ with the largest $P_t[\mathbf{c}]$ are shown as explanation for recommendation of item $t$.

The largest problem of this proposed method is its high computational cost.
To overcome this issue, we give up summing up the probabilities of all the acyclic $s$-$t$ paths and exclude the small probability paths from the summation.
Concretely, we set threshold $\theta$ and we sum up the probabilities of the acyclic $s$-$t$ paths whose probabilities are at least $\theta$.
If $\theta$ is small enough, the calculated probabilities are expected to be good approximations, and the ranking by such approximated probabilities is expected to be correct. 

\begin{algorithm}[tb]
\caption{APBRec($s,T_s$)}
\label{alg1}                          
\begin{algorithmic}[1]
\REQUIRE $s$: vertex for target user, $T_s$: set of recommendable items for user $s$
\ENSURE $t_1,\dots,t_k$: top-$k$ recommended items in $T_s$
\STATE Initialization: for $v\in V$\\
$\text{visited}[v]\gets \text{false}$\\
$\text{recommendable}[v]\gets \begin{cases} \text{true} & (v\in T_s)\\ \text{false} & (v\not\in T_s) \end{cases}$\\
$P_v^{\theta}\gets 0$
\STATE Cal\_Path\_Prob($s,1,0$) \label{calpathprob}
\STATE Calculate items $t=t_1,\dots,t_k$ with the $k$ largest $P_t^{\theta}$
\STATE Output item $t_i$ as the $i$th recommendation
\FOR{$i=1$ to $k$}
\STATE Output color sequences $\mathbf{c}=\mathbf{c}_1,\mathbf{c}_2,\dots,\mathbf{c}_\ell$ with $\ell$ largest $P_{t_i}^{\theta}[\mathbf{c}]$ as explanation
\ENDFOR
\end{algorithmic}
\end{algorithm}

\begin{algorithm}[tb]
\caption{Cal\_Path\_Prob($v, P, d$)}
\label{alg2}                          
\begin{algorithmic}[1]
\REQUIRE $v$: current vertex, $P$: probability of the current $s$-$v$ path, $d$: current path length
\STATE $\text{visited}[v]\gets \text{true}$
\IF{$\text{recommendable}[v]=\text{true}$}
\STATE $P_v^{\theta}\gets P_v^{\theta}+P$, \label{pathprob}
\IF{$P_v^{\theta}[c[0..d-1]]$ is not registered}
\STATE $P_v^{\theta}[c[0..d-1]]\gets 0$
\ENDIF
\STATE $P_v^{\theta}[c[0..d-1]]\gets P_v^{\theta}[c[0..d-1]]+P$\label{csprob}
\ENDIF
\FOR{ $(v',c_d)$ : $(v,v',c_d)\in E$ and $\text{visited}[v']=\text{false}$}\label{pathextension}
\IF{$p((v,v',c_d))\times P\geq \theta$}\label{problargeenough}
\STATE $\mathbf{c}[d]\gets c_d$
\STATE Cal\_Path\_Prob($v', p((v,v',c_d))\times P, d+1$) \label{recursivecall}
\ENDIF
\ENDFOR
\STATE $\text{visited}[v]\gets \text{false}$
\end{algorithmic}
\end{algorithm}

For fixed user $s$, probability sum $P_t^{\theta}$ of all the acyclic $s$-$t$ path with probability at least $\theta$ can be efficiently calculated by the depth first search of $G$. APBRec($s,T_s$), which is shown in Algorithm~\ref{alg1}, is an algorithm that recommends the items $t$ in $T_s$ with the top-$k$ $P_t^{\theta}$ for user $s$.
Furthermore, for each recommended item $t$, the algorithm shows the color sequences $\mathbf{c}$ with top-$\ell$ probability sum $P_t^{\theta}[\mathbf{c}]$ among those of the acyclic $s$-$t$ paths as explanation, where $P_t^{\theta}[\mathbf{c}]$ is the sum of the probabilities of all the acyclic $s$-$t$ paths in $G$ with color sequence $\mathbf{c}$ whose probability is at least $\theta$. The depth first search from $s$ can be done by executing Cal\_Path\_Prob($s,1,0$) at Line~\ref{calpathprob}.
In each recursive call of Cal\_Path\_Prob (Line \ref{recursivecall}), one edge is appended to the current path from $s$.

To prevent the current path from being cyclic, the Boolean variables $\text{visited}[v]$ are set to $\text{true}$ for all the vertices $v$ on the current path,
and the current path is not extended to the vertices with $\text{visited}[v]=\text{true}$ (Line~\ref{pathextension}).
Algorithm Cal\_Path\_Prob($v, P, d$) is given the probability $P$ of current $s$-$v$ path,
  and edge $(v,v',c_d)$ is not appended to the current path unless
  the probability of the path extended by the edge, which can be calculated by $p((v,v',c_d))\times P$, is at least $\theta$ (Line~\ref{problargeenough}). 
The color sequence of the current path is represented by a string $\mathbf{c}=c[0]c[1]\cdots c[d-1]$, which is denoted by $c[0..d-1]$.
In Algorithm Cal\_Path\_Prob($v, P, d$), the current path probability $P$ is added to the acyclic $s$-$v$ paths' probability $P_v^{\theta}$ if $v$ is a vertex corresponding to a recommendable item (Line~\ref{pathprob}). In that case, the probability $P$ is also added to the probability $P_v^{\theta}[c[0..d-1]]$ of its color sequence $c[0..d-1]$ (Line~\ref{csprob}).



\subsection{Explanation by Color Sequences}

As explanation for the recommendation of an item $t$ to a user $s$, we use the color sequences $\mathbf{c}$ of $s$-$t$ paths with high probabilities $P_t^{\theta}[\mathbf{c}]$. Since the item $t$ is recommended to the user $u$ because the probability sum $P_t^{\theta}$ of the $s$-$t$ paths is high,
showing the color sequences $\mathbf{c}$ that are contributed to high $P_t^{\theta}$ is reasonable explanation in the context of transparency considering the fact that 
$P_t^{\theta}=\sum_{\mathbf{c}}P_t^{\theta}[\mathbf{c}]$.
Furthermore, color sequences are relation paths from the recommended user to the recommended item, which clarifies what his/her actions (watching, rating, tagging and so on) strongly connect to the item through what other relations between objects.
For example, color sequences can reveal that the recommended movie is a movie of the same genre as a movie the user has watched in the past, or a movie watched by other users who have watched the same movie as the user watched.

\section{Experiments}

We conduct experiments to compare the recommendation performance of our proposed algorithm APBRec with those of the state-of-the-art methods
using two real-world datasets. We also check the effectiveness of high-probability color sequences as recommendation explanation.

\subsection{Experimental Setup}

We set the threshold $\theta$ used in Cal\_Path\_Prob to $10^{-7}$ in our experiment.
Other settings are described below.

\subsubsection{Datasets}

The datasets we use in our experiments are Movielens 20M \cite{movielens} and Food.com Recipes and Interactions\footnote{https://www.kaggle.com/shuyangli94/food-com-recipes-and-user-interactions}, which were collected on a movie review site and a recipe aggregator site, respectively.
Both the datasets are composed of rating and tagging data whose statistics are shown in Table~\ref{tab:movielens}.
The rating scales of Movielens and Food.com\footnote{The scale of the original dataset is 0-5 and  it contains users who rated less than 20 recipes. We shifted the scale by one to use the same weight function and removed users who rated less than 20 recipes.} are 1-5 and 1-6, respectively, and every user in both the datasets has at least 20 ratings.
The datasets contain tags which are words tagged to movies and recipes by users.

We randomly divide the set of ratings into 60$\%$ of the ratings for training and the other 40$\%$ for testing.
We report the average results over five such random splits. 


\begin{table}[tb]
  \caption{Dataset statistics}
  \centering
  \label{tab:movielens}
  \begin{tabular}{lccccc}
    \toprule
    Dataset &$\#$users&$\#$items&$\#$tags&$\#$ratings&$\#$taggings\\
    \midrule
    Movielens 20M & 138493 & 27278 & 38644 & 20000263 & 465564\\
    Food.com Recipes and Interactions & 6389 & 197317 & 532 & 719548 & 2798545\\ 
  \bottomrule
\end{tabular}
\end{table}

\subsubsection{Graphs Construction}

We construct the following edge-colored graph $G(V,E,p)$ from two datasets.
The vertex set $V$ is the set of all the objects, that is, users, items and tags.
We abuse notation and let user $u$, item $m$, tag $t$ represent the corresponding vertices themselves. 
The edge set $E$ is constructed from ratings and taggings as follows.
For each rating of item $m$ by user $u$, two colored directed edges $(u,m,\mathbf{R})$ and $(m,u,\mathbf{r})$ are created. 
For each tagging of item $m$ with tag $t$ by user $u$, six colored directed edges
$(u,m,\mathbf{T})$, $(m,u,\mathbf{t})$, $(u,t,\mathbf{U})$, $(t,u,\mathbf{u})$, $(m,t,\mathbf{M})$ and $(t,m,\mathbf{m})$
are created. So, the color set is $C=\{\mathbf{R},\mathbf{r},\mathbf{T},\mathbf{t},\mathbf{U},\mathbf{u},\mathbf{M},\mathbf{m}\}$,
and the relation that each color represents is shown in Table~\ref{tab:cedge}.


\begin{table}[tb]
  \caption{Colored edges in $G(V,E,p)$ for the datasets, their related information that is used for determining their weight, and representing relations. We let $r_{u,m}$ denote the rating value of item $m$ by user $u$, and let $t_{u,m}$ denote the set of tags to item $m$ tagged by user $u$. The notation $|\cdot|$ for set `$\cdot$' means the number of elements in set `$\cdot$'.}\label{tab:cedge}
  {\small
  \scalebox{0.8}{
  \begin{tabular}{@{}ccp{4.5cm}|ccl@{}}
    \toprule
    edge  & rel. inf.   & relation & edge & rel. inf. & relation\\
    \midrule
$(u,m,\mathbf{R})$ & $r_{u,m}$ & user $u$ rated item $m$ at $r_{u,m}$. & $(u,t,\mathbf{U})$ & $|\{m \mid t\in t_{u,m}\}|$ &  user $u$ tagged with tag $t$.\\
$(m,u,\mathbf{r})$ & $r_{u,m}$ & item $m$ was rated at $r_{u,m}$ by user $u$. & $(t,u,\mathbf{u})$ & $|\{m \mid t\in t_{u,m}\}|$ & with tag $t$ user $u$ tagged.\\
    $(u,m,\mathbf{T})$ & $|t_{u,m}|$ & user $u$ tagged item $m$. & $(m,t,\mathbf{M})$ & $|\{u \mid t\in t_{u,m}\}|$ & item $m$ was tagged with tag $t$.\\
$(m,u,\mathbf{t})$ & $|t_{u,m}|$ & item $m$ was tagged by user $u$. & $(t,m,\mathbf{m})$ & $|\{u \mid t\in t_{u,m}\}|$ & with tag $t$ item $m$ was tagged.\\
    \bottomrule
\end{tabular}
}
}
\end{table}

To construct a function $w_c$ for each color $c$,
we apply the same normalization used for rating values in \cite{MRH},  which is defined as follows.
Let $w_{v_1,v_2,c}$ denote the related information for edge $(v_1,v_2,c)$ shown in the column `rel. inf.' of Table~\ref{tab:cedge}.
Then, the function $w_c:E\rightarrow (0,\infty)$ for color $c\in C$ is defined as 
\[
w_c((v_1,v_2,c))= \frac{w'((v_1,v_2,c))}{\mathrm{ave}(w'((v_1,\cdot,c)))},
\]
where $\mathrm{ave}(w'((v_1,\cdot,c)))$ is the average of $w'((v_1,v,c))$ over $\{v\mid (v_1,v,c)\in E\}$, 
and the function $w'$ is defined as
\[
w'((v_1,v_2,c))=w'((v_2,v_1,\bar{c}))=\frac{w_{v_1,v_2,c}}{\sqrt{\displaystyle\sum_{v_2':(v_1,v_2',c)\in E}w_{v_1,v_2',c}}\sqrt{\displaystyle\sum_{v_1':(v_2,v_1',\bar{c})\in E}w_{v_1',v_2,\bar{c}}}},
\]
where $\bar{c}$ is the color representing the reverse relation to the relation represented by color $c$, that is, $\bar{\mathbf{R}}=\mathbf{r}$, $\bar{\mathbf{T}}=\mathbf{t}$, $\bar{\mathbf{U}}=\mathbf{u}$ and $\bar{\mathbf{M}}=\mathbf{m}$. The edge weight function $p$ is
a normalized $w_c$ so as to satisfy $\sum_{(v,c):(v_1,v,c)\in E}p((v_1,v,c))=1$, that is,
\[
p((v_1,v_2,c))=\frac{w_c((v_1,v_2,c))}{\sum_{(v,c):(v_1,v,c)\in E}w_c((v_1,v,c))}.
\]

\subsubsection{Evaluation Metrics}

We evaluate recommendation performance by nDCG (normalized Discounted Cumulative Gain) \cite{Burges2005}, Precision, Recall and F1-measure.
DCG(Discounted Cumulative Gain) for recommended item ranking $m_1,m_2,\cdots$ to user $u$ is a ranking metric that discounts the score towards the bottom, that is, 
\[
DCG@n=\sum_{i=1}^n\frac{2^{r_{u,m_i}}-1}{\log_2(i+1)},
\]
where $r_{u,m_i}$ is the rating value of the $i$th item $m_i$ in the recommended ranking for user $u$, and $n$ is the maximum rank used for the evaluation.
nDCG is a normalized DCG that is the DCG divided by the IDCG (Ideal DCG), where the IDCG is the DCG for the item $m$ ranking that is sorted in descending order of $r_{u,m}$.

Precision, Recall and F1-measure were used to measure recommendation performance for high-rated items, which are defined as the items rated at least 4 in Movielens and at least 5 in Food.com.
Let $M^{\mathrm{high}}_u$ denote the set of items rated high by user $u$ and let $M^{\mathrm{rec}}_u(n)$ be the set of top n recommended items. $\mathrm{Precision@}n$, $\mathrm{Recall@}n$ and $\mathrm{F1@}n$ for user $u$ are defined as
\begin{align*}
\mathrm{Precision@}n=&\frac{|M^{\mathrm{high}}_u\cap M^{\mathrm{rec}}_u(n)|}{n},\ \ 
\mathrm{Recall@}n=\frac{|M^{\mathrm{high}}_u\cap M^{\mathrm{rec}}_u(n)|}{|M^{\mathrm{high}}_u|}, \text{ and }\\
\mathrm{F1@}n=&\frac{2\mathrm{Precision@}n\cdot \mathrm{Recall@}n}{\mathrm{Precision@}n+\mathrm{Recall@}n}.
\end{align*}
They take values between 0 and 1, and the closer to 1, the better the recommendation performance is. 

\subsubsection{Comparison Methods}

We compare the accuracy of the proposed method with those of baseline and state-of-the-art models. We used four models,
AVE, IBCF, MRH and BPR models. 
AVE model is a baseline model, which ranks items by the ratings averaged over all the users who rated the item.
IBCF model is an item-based collaborative filtering \cite{sarwar2001item}, which predicts rating $r_{u,m}$ by
\[
\frac{\sum_{m'\in M_u} \mathrm{cosine}(r_{\cdot,m},r_{\cdot,m'})r_{u,m}}{\sum_{m'\in M_u} \mathrm{cosine}(r_{\cdot,m},r_{\cdot,m'})},
\]
where
\[
\mathrm{cosine}(r_{\cdot,m},r_{\cdot,m'})=\frac{\sum_{u\in U_m\cap U_{m'}}r_{u,m}r_{u,m'}}{\sqrt{\sum_{u\in U_m\cap U_{m'}}r_{u,m}^2}\sqrt{\sum_{u\in U_m\cap U_{m'}}r_{u,m'}^2}},
\]$M_u$ is the set of movies rated by user $u$, and $U_m$ is the set of users who rated item $m$.
BPR (Bayesian Personalized Ranking) model is a kind of matrix factorization \cite{rendle2012bpr}, which optimizes the posterior probability of correctly ranking item pairs for each user.
We use the implementation shown at http://ethen8181.github.io/machine-learning/recsys/4\_bpr.html with default parameter values except the following parameters: learning\_rate=0.1, n\_iters=3000, n\_factors=60 and batch\_size=100.
MRH (Music Recommendation via Hypergraph) model is a graph-based method that uses social information to make recommendations \cite{MRH}.
We construct a hypergraph using rating and tagging information and make a transition matrix $A$ for Random Walks with Restarts version of MRH model.
The stationary distribution $\lim_{t\rightarrow \infty}\mathbf{f}^{(t)}$ for $\mathbf{f}^{(t+1)}=\alpha A\mathbf{f}^{(t)}+(1-\alpha)\mathbf{y}_u$ is used for recommendation to user $u$, where $\mathbf{y}_u$ is a query vector for user $u$ and whose nonzero entries are the entry corresponding to $u$, which is set to $1$,
and the entries corresponding to $v$ that is connected to $u$ in the graph, which are set to $A_{u,v}$.
In the experiment, the restart probability $1-\alpha$ is set to $0.04$, and the stationary distribution is approximated by $\mathbf{f}^{(t)}$ for $t=80$.

\subsection{Results}

\subsubsection{Recommendation Performance}
The nDCG@10, Precision@10, Recall@10 and F1@10 averaged over all users and 5 runs with 95\% confidence intervals for the five methods are shown in Table~\ref{tab:expresult}.
APBRec outperforms the baseline method AVE and has comparable performance to 
the other three non-explainable state-of-the-art recommendation methods.
\begin{table}[tb]
  \caption{Recommendation performance of the five recommendation models for two datasets by nDCG@10, Precision@10, Recall@10 and F1@10. The values are averaged over all users and 5 runs and 95\% confidence intervals are parenthesized. The highest values are bolded and those which are not statistically significantly different from them are italicized.}
  \centering
  \label{tab:expresult}
  \begin{tabular}{llllll}
    \toprule
    dataset&method& nDCG & Precision & Recall & F1\\
    \midrule
    \multirow{5}{*}{Movielens}
    &AVE& 0.4508$\ci{0.0003}$ &0.3658$\ci{0.0006}$ &0.3625$\ci{0.0002}$ & 0.3106$\ci{0.0004}$\\
    &IBCF& \textbf{0.7667}$\ci{0.0001}$& \textbf{0.6901}$\ci{0.0005}$&\textbf{0.5239}$\ci{0.0005}$&\textbf{0.4926}$\ci{0.0004}$\\
    &MRH& 0.7354$\ci{0.0003}$ &0.6478$\ci{0.0004}$ &0.4991$\ci{0.0005}$ &0.4657$\ci{0.0004}$  \\
    &BPR& 0.6885 $\ci{0.0047}$& 0.6123 $\ci{0.0026}$& 0.4770$\ci{0.0016}$ & 0.4419$\ci{0.0018}$ \\
    &APBRec& 0.7023 $\ci{0.0002}$&0.6241 $\ci{0.0004}$&0.4799 $\ci{0.0003}$&0.4474 $\ci{0.0003}$ \\
    \midrule
    \multirow{5}{*}{Food.com}
    &AVE&0.8328$\ci{0.0011}$ & 0.8587$\ci{0.0014}$&0.5673$\ci{0.0018}$&0.6094$\ci{0.0017}$\\
    &IBCF& 0.8745$\ci{0.0012}$ & \textit{0.8785}$\ci{0.0018}$ & \textit{0.5755}$\ci{0.0016}$ & \textit{0.6199}$\ci{0.0018}$\\
    &MRH& 0.8659$\ci{0.0012}$&0.8743 $\ci{0.0021}$&\textit{0.5740}$\ci{0.0014}$ & \textit{0.6179}$\ci{0.0020}$\\
    &BPR&\textbf{0.8804}$\ci{0.0008}$ & \textbf{0.8806}$\ci{0.0018}$& \textbf{0.5761}$\ci{0.0011}$&\textbf{0.6208}$\ci{0.0015}$\\
    &APBRec&0.8735$\ci{0.0009}$ &0.\textit{8777}$\ci{0.0019}$ & \textit{0.5752}$\ci{0.0013}$&\textit{0.6196}$\ci{0.0018}$\\
  \bottomrule
\end{tabular}
\end{table}

\subsubsection{Explainability}

Two users' top-10 recommendation lists by APBRec for Food.com dataset are shown in Table~\ref{tab:reclist}.
As explanation of each recommended recipe, three highest-probability color sequences are shown.
The color sequences give contexts of the recommendation and makes the recommendation more transparent.
For example, color sequence ``$\mathbf{RrR}$'' can be interpreted as ``the recipes rated high by other users who highly rated your high-rating recipes''. You can see tag's contribution to the recommendation lists.
Color sequence ``$\mathbf{Um}$'' is the highest ranked color sequence for five recipes recommended to user 231.
Interpretation of ``$\mathbf{Um}$'' can be  ``the recipes tagged with the same tag that you tagged to other recipes''.

\begin{table}[tb]
  \caption{Top-10 recommended recipes to users 133 and 231 by APBRec in Food.com dataset. The three highest probability color sequences are also shown for each recommended recipe.}
  \label{tab:reclist}
   \scalebox{0.9}{
  \begin{tabular}{l|rllll}
    \toprule
    \multirow{2}{*}{user}&\multirow{2}{*}{rank} &\multirow{2}{*}{recipe}& \multicolumn{3}{c}{color sequence ranking}\\
    \cline{4-6}
    &&&1st$(\%)$&2nd$(\%)$&3rd$(\%)$\\
    \midrule
    \multirow{10}{*}{133}&1& javanese roasted salmon and wilted spinach& $\mathbf{RrR}(92.3)$ & $\mathbf{TrR}(7.6)$ & \\
    &2 &beer& $\mathbf{RrR}(91.6)$ & $\mathbf{TrR}(8.3)$ & \\ 
    &3&chilli beer damper& $\mathbf{RrR}(94.2)$ & $\mathbf{TrR}(5.7)$ & \\
    &4&meat pinwheels& $\mathbf{TrR}(68.4)$ & $\mathbf{RtR}(31.5)$ &\\
    &5&spicy calamari with bacon and scallions& $\mathbf{RrR}(86.5)$ & $\mathbf{TrR}(12.3)$&$\mathbf{Um}(1.0)$ \\
    &6&roasted green beans with garlic and pine nuts&$\mathbf{RrR}(91.7)$ & $\mathbf{TrR}(5.6)$&$\mathbf{RMm}(2.5)$ \\
    &7&bubble and squeak& $\mathbf{RrR}(92.5)$ & $\mathbf{TrR}(7.4)$ & \\
    &8&tamarind sauce& $\mathbf{TrR}(100)$ &  & \\
    &9&citrus morning sunrise& $\mathbf{TrR}(96.5)$ & $\mathbf{RrR}(3.4)$& \\
    &10&maple sugar pumpkin pie& $\mathbf{TmM}(68.7)$ & $\mathbf{Um}(31.2)$&\\
    \midrule
    \multirow{10}{*}{231}&1& baked sausage stuffed jumbo pasta shells& $\mathbf{Um}(63.2)$ & $\mathbf{TMm}(33.1)$ &$\mathbf{Uut}(1.2)$ \\
    &2 &greek spinach rice balls& $\mathbf{RrR}(100)$ &   \\ 
    &3&raspberry wine toaster bread& $\mathbf{RrR}(100)$ &  \\
    &4&crunchy poppy seed chicken salad& $\mathbf{RrR}(100)$ &  \\
    &5&salt rising bread& $\mathbf{RrR}(100)$ &  \\
    &6&garlic lime chicken&$\mathbf{Um}(62.8)$ & $\mathbf{TMm}(32.8)$ &$\mathbf{RMm}(3.1)$  \\
    &7&the best mexican tortilla roll ups& $\mathbf{RrR}(93.7)$ & $\mathbf{TrR}(5.3)$ &$\mathbf{Um}(0.9)$ \\
    &8&quick fish stew&$\mathbf{Um}(62.8)$ & $\mathbf{TMm}(32.8)$ &$\mathbf{RMm}(3.1)$  \\
    &9&mustard herb flank steak& $\mathbf{Um}(63.0)$ & $\mathbf{TMm}(32.9)$ &$\mathbf{RMm}(3.2)$  \\
    &10&caramel apple cupcakes& $\mathbf{Um}(63.5)$ & $\mathbf{TMm}(33.2)$ &$\mathbf{RMm}(3.2)$\\
    \bottomrule 
\end{tabular}
}
\end{table}

If the highest-probability color sequences are different, the main recommendation reasons are different.
Thus, we can expect to get a different type recommendation list through filtering recommended items by the highest-probability color sequences.
The recommendation list to user 1741 of Movielens dataset by APBRec in Table~\ref{tab:tag-relatedcolor} supports this expectation.
The first and last sublists are divided depending on whether the item's highest-probability color sequence is $\mathbf{RrR}$ or not.
The genre distributions of the two sublists look different:
genres Sci-Fi, Film-Noir, Mystery and War appear in the first sublist only while
genres Children, Musical, Horror, Action, Adventure and IMAX appear in the last sublist only.
\begin{table}[tb]
  \caption{The top 30 movies recommended to user 1741 by APBRec in Movielens dataset.
    The first and last sublists are divided depending on whether the item's highest-probability color sequence is $\mathbf{RrR}$ or not.
    The italic genres indicate sublist-uniqueness, that is, they do not appear in the other sublist. }
  \label{tab:tag-relatedcolor}
  \scalebox{0.8}{
  \begin{tabular}{rlp{6.6cm}lll}
    \toprule
    \multicolumn{2}{l}{\multirow{2}{*}{rank movie}}& \multirow{2}{*}{genres}&\multicolumn{3}{c}{color sequence ranking}\\
    \cline{4-6}
    &&&1st&2nd&3rd\\
    \midrule
     1&The Man Who Wouldn't Die&Crime,Drama,Thriller& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     2&Slam Dance&Thriller& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     3&Shepherd&\textit{Sci-Fi}& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     9&Dragon Age&Animation,Fantasy& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     16&Lady on a Train&Comedy,Crime,\textit{Film-Noir},\textit{Mystery},Romance, Thriller&$\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     17&Long Night's Journey Into Day&Documentary& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     20&Jim Jefferies Fully Functional&Comedy& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     22&Napoleon&Drama,\textit{War}& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     26&Big Parade&Drama,Romance,\textit{War}& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     27&Fawlty Towers&Comedy& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
    \midrule
      4& Momo& Animation,\textit{Children},Fantasy& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuR}$\\
      5& Houseboat& Comedy,Romance& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      6& Pina& Documentary,\textit{Musical}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuR}$\\
      7& Alvin and the Chipmunks& \textit{Children},Comedy& $\mathbf{Um}$ & $\mathbf{RMm}$&$\mathbf{TrR}$\\
      8& The Wolf Brigade& Animation,Fantasy,Thriller& $\mathbf{Um}$
      &$\mathbf{RrR}$&$\mathbf{TrR}$\\
      10& My Name Is Bruce& Comedy,\textit{Horror}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      11& Party Monster& Comedy,Crime,Drama,Thriller& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      12& Out of Africa& Drama,Romance& $\mathbf{Um}$ & $\mathbf{RrR}$&$\mathbf{TtR}$\\
      13& Shattered Glass& Crime,Drama& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      14& Bat& \textit{Horror}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuR}$\\
      15& Children of the Corn& \textit{Horror}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UmrR}$\\
      18& V.I.P.s& Drama& $\mathbf{TrR}$ & $\mathbf{UmrR}$&$\mathbf{TrRrR}$\\
      19& A Question of Faith& Drama& $\mathbf{Um}$ & $\mathbf{UmMm}$&$\mathbf{TMm}$\\
      21& Double Dynamite& Comedy,\textit{Musical}& $\mathbf{TrR}$ & $\mathbf{UmrR}$&\\
      23& Big Fella& Drama,\textit{Musical}& $\mathbf{TrR}$ & $\mathbf{UmrR}$&\\\
      24& The Guardian& \textit{Action},Thriller& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UmrR}$\\
      25& The Killer Whale& \textit{Action},Drama,\textit{Horror},Thriller& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      28& Bella& Drama,Romance& $\mathbf{Um}$ & $\mathbf{RrR}$&$\mathbf{TrR}$\\
      29& How to Train Your Dragon& \textit{Adventure},Animation,\textit{Children},Fantasy,\textit{IMAX}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuUm}$\\
      30& Art and Craft& Documentary& $\mathbf{Um}$ & $\mathbf{UuR}$&$\mathbf{TMm}$\\
    \bottomrule
\end{tabular}
}
\end{table}


\section{Conclusion}
We proposed a recommendation algorithm that shows high-probability color sequences (high-contributed relation path) as explanation for recommendation. To calculate high-probability color sequences, our algorithm does depth first search on an edge-colored directed graph that represents various relations between recommendation-related objects.
The probability sum of all the above-threshold-probability acyclic $s$-$t$ paths are used for the recommendation score of item $t$ for user $s$.
According to the experiments using real-world datasets, the recommendation performance of our proposed method is comparable to non-explainable state-of-the-art recommendation methods.
Color sequences are confirmed to be useful not only for recommendation explanation but also for realizing recommendation lists of various trends.

\section*{Acknowledgments}

This work was partially supported by JST CREST Grant Number JPMJCR18K3, Japan.


%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
\bibliographystyle{splncs04}
\bibliography{mybibliography}
%
\end{document}
