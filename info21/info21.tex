%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,10.5pt]{jsarticle}           % pLaTeX の場合
\usepackage{suri2020}
\usepackage{authblk}

\usepackage[dvipdfmx]{graphicx} 
\usepackage{amsmath,amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{algorithm}
\usepackage{algorithmic}
%\usepackage{algpseudocode}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{bm}
\usepackage{color}
\usepackage{latexsym}
\usepackage{comment}
\newcommand{\argmin}{\mathop{\rm arg~min}\limits}
\bibliographystyle{junsrt}


\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}
\renewcommand{\textfraction}{0.0}
\def\ci#1{\scriptstyle  (\pm #1)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\author{茅根宏介\Email{chinone@ist.hokudai.ac.jp}
\hspace{5mm}中村篤祥\\
(北大情報科学)\contactto{札幌市北区北14条西9丁目北海道大学大学院情報科学研究科}}

\title{有向グラフの非巡回パスに基づいた説明可能な推薦システム}

\begin{document}
\maketitle

\section{はじめに}
推薦システムとは多くのウェブサービスで利用されており, ユーザの好みに合ったアイテムを提示するシステムである. 近年, 研究において説明可能性という観点で注目されている\cite{zhang2018explainable}.
説明可能な推薦システムは, アイテムを推薦するだけでなく, 推薦理由も提示するシステムである. また説明可能性は, システムの透明性や有効性, ユーザの満足度などを向上させる.
古典的な推薦手法であるユーザやアイテムの類似性に基づく協調フィルタリング\cite{aggarwal1999horting,sarwar2001item}では, 「そのアイテムは，あなたの購入履歴と類似したユーザによって購入されました」や「そのアイテムは，あなたが購入したアイテムと類似しています」のような説明が可能である. 
しかし, 予測精度の高い推薦モデルとされている潜在的な因子を行列分解によって学習しアイテムの評価値を予測するモデル\cite{koren2008factorization}や深層学習モデル\cite{wang2015collaborative}などは, 推薦の理由を直感的に理解することが難しいとされている.
一方で, ユーザやアイテムなどの関連するオブジェクト間のさまざまな関係を利用して, 推薦の精度や説明可能性を向上させる取り組みも行われている.
このようなオブジェクト間の関係は, オブジェクトを頂点とし, オブジェクト間の関係を辺で表すグラフで表現することができる. グラフの各関係に基づく嗜好伝播度を考慮し, ユーザに対するアイテムの推定嗜好度を計算することで, ユーザにアイテムを推薦することができる\cite{MRH}.
このグラフベースの手法の利点は, 推定された嗜好度に最も貢献している経路を推薦の説明として示すことができる点である. これまでの研究では, このような嗜好度の推定は, 嗜好伝播度に対応する遷移確率を持つマルコフ過程の定常分布を計算することで行われていた. 
この手法の近似計算は, 行列の乗算を繰り返すことで効率的に行われるが, 推定された嗜好度の高いアイテムに貢献した経路を得ることはできない.
本論文では, 推薦に関連するオブジェクト間の様々な関係から構築される有向グラフの非巡回パスに基づく新しい推薦アルゴリズムを提案する. 有向グラフにおいて, 辺に色を保持させ, 辺の色が, その両端の頂点に対応するオブジェクト間の関係を表す. 本アルゴリズムは, 適切な辺の伝搬確率と閾値$\theta$が与えられたとき、確率が少なくとも$\theta$以上であるすべての非巡回パス$s$-$t$の確率和を計算する. その際, パス$s$-$t$の各色配列の確率の合計も計算する. そして, 高確率のアイテム$t$は, パス$s$-$t$の高確率の色配列を伴ってユーザ$s$に推薦される. 
このことから, ユーザ$s$に対するアイテム$t$の推薦の主な推薦理由を知ることができる. 
実世界のデータセットを用いた実験結果によると, 提案手法の推薦精度は, 説明不可能な最新の推薦手法と同等である. また, 最も確率の高い色配列で推薦リストをチェックすることで, 色配列が推薦の説明に適していることが確認された. さらに, 色配列の異なる推薦リストの間には異なる傾向が見られ, 色配列を用いて異なる傾向の推薦リストを提供できる可能性が示された.

\subsection*{関連研究}
説明可能な推薦システムという言葉が使われ始めたのは最近だが, そのような推薦システムは以前から存在していた\cite{zhang2014explicit}. 
ユーザベースやアイテムベースの協調フィルタリングは, ユーザやアイテムとの類似性に基づいてアイテムを推薦するため, 説明可能な推薦システムの一種であると言える.\cite{aggarwal1999horting,herlocker2000explaining,sarwar2001item}
Explicit Factor Model (EFM)\cite{chen2016learning,zhang2015daily}は, Latent Factor Model (LFN)\cite{koren2008factorization}を説明可能なように改変して作られた説明可能な推薦手法である.
また, CNN\cite{seo2017interpretable}やRNN\cite{donkers2017sequential}などの深層学習を用いた推薦では, 自然言語生成モデルによる説明文の自動生成に取り組んでいるものがある\cite{li2017neural}. 
潜在因子モデルや深層学習モデルで生成された推薦は解釈することは難しいが重要な問題であると言われている\cite{zhang2018explainable}.
最近の説明可能な推薦システムでは, ユーザとアイテムの間にある購入履歴やレビュー情報, 友人関係などの情報を利用している\cite{papadimitriou2012generalized,MRH}. 
また、ユーザやアイテムに関する豊富な情報を持つナレッジグラフを用いて, 直感的でより適切な説明を生成する技術もある\cite{catherine2017explainable}.

\section{予備知識}
有向グラフ$G$は三つ組$G(V,E,p)$で, $V$は頂点の集合, $E\subseteq V\times V\times C$は色集合$C$を持つ有向辺の集合, $p$は辺の重み関数$p:E\rightarrow (0,\infty)$である.
なお, 色$c_1, c_2$が異なっていれば, uv間には$(u,v,c_1), (u,v,c_2)\in V\times V\times C$の複数の辺が存在する. 
辺の重み関数は, $u\in V$のすべてに対して$\sum_{(v,c):(u,v,c)\in E}p((u,v,c)) =1$という確率条件を満たす関数$p$のみを考える. 
グラフ$G$の非巡回パス$s$-$t$は, 辺の配列$(s,v_1,c_1)(v_1,v_2,c_2)\cdots (v_{k-1},v_k,c_k)(v_k,t,c_{k+1})$として定義される. このとき, $k\leq |V|$に対して$s, v_1, v_2, \dots, v_k,t$は互いに異なっていなければならない.
また, パス$s$-$t$である$(s,v_1,c_1)(v_1,v_2,c_2)\cdots (v_{k-1},v_k,c_k)(v_k,t,c_{k+1})$に対して, $c_1c_2\cdots c_{k+1}$をパス$s$-$t$の色配列と呼ぶことにする.
また, パス$s$-$t$の確率を$p((s,v_1,c_1))\times p((v_1,v_2,c_2)) \times\cdots\times p((v_{k-1},v_k,c_k))\times p((v_k,t,c_{k+1}))$と定義する.

\section{提案手法}
本節では, 有向グラフの非巡回パスに基づいた説明可能な推薦モデルを提案する. まず, 推薦に用いられる情報を有向グラフで表現し, 非巡回パスに基づく推薦方法を紹介する. そして, 色配列による推薦の説明を行う.

\subsection{グラフ構築}
評価情報やタグ情報, アイテムの属性などの様々な情報を用いた推薦のために, 重み付き有向グラフ$G(V,E,p)$を構築する. $V$をユーザやアイテム, タグなどの推薦に用いられる情報全てのオブジェクトの集合とする. 
色付きの有向辺$(u,v,c)$は, オブジェクト$u$がオブジェクト$v$とある関係 (色$c$で表される)を持つ場合に生成される. 
例えば, 評価関係には色$\mathbf{R}$を用いると, ユーザ$u$がアイテム$v$を評価した場合, 辺$(u,v,\mathbf{R})$が生成される. 
有向グラフを扱っているので, 逆の関係には別の色を使っている. 
それゆえに, $\mathbf{r}$の色で表される評価されたという関係を考慮した場合, 辺$(v,u,\mathbf{r})$も作成される.
また, タグ関係に$\mathbf{T}$という色が使われているとすると, ユーザ$u$がアイテム$v$に何らかのタグを付けた場合に辺$(u,v,\mathbf{T})$が生成されるが, これは評価関係の辺$(u,v,\mathbf{R})$の生成とは独立に行われる.
辺の重み関数$p((u,v,c))$は, オブジェクト$u$がオブジェクト$v$をどれだけ好んでいるかを表す値に設定される.
そのよう嗜好伝搬度は, 辺の色$c$に対応する関係に依存する.
まず, 各色$c$について, それが表現する関係に応じた関数$w_c:E\cap(V\times V\times \{c\})\rightarrow (0,\infty)$を構築する. 
そして, すべての色$c$に対して$w_c$を正規化することで重み関数$p$が構築される.

\subsection{Acyclic-Path-Based Recommendation}
有向グラフ$G(V,E,p)$上の非巡回パス$s$-$t$に基づいて, ユーザ$s$にアイテム$t$を推薦する推薦法を提案する. 
各ユーザ$s$に対して, アイテム$t$ごとに$G$上のすべてのパス$s$-$t$の確率の総和$P_t$を計算し, $P_t$が最大のアイテム$t$を推奨する.
この非巡回パスに基づく推薦の利点は, 高確率パスを推薦の説明として示すことができることである.
この情報として, 高確率の非巡回パス$s$-$t$の色配列を用いる。
各アイテム$t$に対して、色配列$\mathbf{c}$を持つ$G$の全ての非巡回パス$s$-$t$の確率の総和$P_t[\mathbf{c}]$を出現する色配列$\mathbf{c}$ごとに計算し, $P_t[\mathbf{c}]$が最大の色配列$\mathbf{c}$をアイテム$t$の推薦の説明として提示する. 
この提案手法の最大の問題点は, 計算コストが高いことである. この問題を解決するために, 非巡回パス$s$-$t$の確率をすべて計算することをあきらめ, 確率の小さいパスを計算から除外する. 具体的には, 閾値$\theta$を設定し, 確率が$\theta$以上の非巡回パス$s$-$t$の確率を考える. $\theta$が十分に小さければ, 計算された確率は良い近似値であると考えられ, その近似値による順位付けは正しいと考えられる. 

\begin{algorithm}[tb]
\caption{APBRec($s,T_s$)}
\label{alg1}                          
\begin{algorithmic}[1]
\REQUIRE $s$: vertex for target user, $T_s$: set of recommendable items for user $s$
\ENSURE $t_1,\dots,t_k$: top-$k$ recommended items in $T_s$
\STATE Initialization: for $v\in V$\\
$\text{visited}[v]\gets \text{false}$\\
$\text{recommendable}[v]\gets \begin{cases} \text{true} & (v\in T_s)\\ \text{false} & (v\not\in T_s) \end{cases}$\\
$P_v^{\theta}\gets 0$
\STATE Cal\_Path\_Prob($s,1,0$) \label{calpathprob}
\STATE Calculate items $t=t_1,\dots,t_k$ with the $k$ largest $P_t^{\theta}$
\STATE Output item $t_i$ as the $i$th recommendation
\FOR{$i=1$ to $k$}
\STATE Output color sequences $\mathbf{c}=\mathbf{c}_1,\mathbf{c}_2,\dots,\mathbf{c}_\ell$ with $\ell$ largest $P_{t_i}^{\theta}[\mathbf{c}]$ as explanation
\ENDFOR
\end{algorithmic}
\end{algorithm}

\begin{algorithm}[tb]
\caption{Cal\_Path\_Prob($v, P, d$)}
\label{alg2}                          
\begin{algorithmic}[1]
\REQUIRE $v$: current vertex, $P$: probability of the current $s$-$v$ path, $d$: current path length
\STATE $\text{visited}[v]\gets \text{true}$
\IF{$\text{recommendable}[v]=\text{true}$}
\STATE $P_v^{\theta}\gets P_v^{\theta}+P$, \label{pathprob}
\IF{$P_v^{\theta}[c[0..d-1]]$ is not registered}
\STATE $P_v^{\theta}[c[0..d-1]]\gets 0$
\ENDIF
\STATE $P_v^{\theta}[c[0..d-1]]\gets P_v^{\theta}[c[0..d-1]]+P$\label{csprob}
\ENDIF
\FOR{ $(v',c_d)$ : $(v,v',c_d)\in E$ and $\text{visited}[v']=\text{false}$}\label{pathextension}
\IF{$p((v,v',c_d))\times P\geq \theta$}\label{problargeenough}
\STATE $\mathbf{c}[d]\gets c_d$
\STATE Cal\_Path\_Prob($v', p((v,v',c_d))\times P, d+1$) \label{recursivecall}
\ENDIF
\ENDFOR
\STATE $\text{visited}[v]\gets \text{false}$
\end{algorithmic}
\end{algorithm}

あるユーザ$s$に対して, $G$上の深さ優先探索によって, 少なくとも確率$\theta$を持つすべての非巡回パス$s$-$t$の確率和$P_t^{\theta}$を効率的に計算できる.
Algorithm~\ref{alg1}に示したAPBRec($s,T_s$)は, ユーザ$s$に対して上位$k$個の$P_t^{\theta}$を持つ集合$T_s$の中のアイテム$t$を推薦するアルゴリズムである. 
さらに, 推薦されたアイテム$t$ごとに, $s$-$t$間の非巡回パスの中で, $P_t^{\theta}[\mathbf{c}]$の確率和がトップ$\ell$の色配列$\mathbf{c}$を説明として表示する.
ここで, $P_t^{\theta}[\mathbf{c}]$は、$G$の中の色配列$\mathbf{c}$を持つ非巡回パス$s$-$t$のうち, 確率が$\theta$以上のものの確率の総和である. 
$s$からの深さ優先探索は, Line~\ref{calpathprob}のCal\_Path\_Prob($s,1,0$)を実行する. 
Cal\_Path\_Prob (Line \ref{recursivecall})を再帰的に呼び出すごとに, $s$からの現在のパスに辺が1本追加される. 
また, 現在のパスが巡回しないように, 現在のパス上のすべての頂点$v$のブール変数$\text{visited}[v]$を$\text{true}$に設定し, $\text{visited}[v]=\text{true}$ (Line~\ref{pathextension})の頂点には現在のパスから延ばさない.
アルゴリズムCal\_Path\_Prob($v, P, d$)は, 現在のパス$s$-$v$の確率$P$が与えられ, $p((v,v',c_d))\times P$で計算される延長したパスの確率が$\theta$以上でない限り, 辺$(v,v',c_d)$は現在のパスに追加されない (Line~\ref{problargeenough}).
現在のパスの色配列は, 文字列$\mathbf{c}=c[0]c[1]\cdots c[d-1]$で表され, これを$c[0..d-1]$で定義する. 
Cal\_Path\_Prob($v, P, d$)では, $v$がおすすめアイテムに対応する頂点である場合 (Line~\ref{pathprob}), 現在のパスの確率$P$を, 非巡回パス$s$-$v$の確率$P_v^{\theta}$に加える. その場合, 確率$P$はその色配列$c[0..d-1]$の確率$P_v^{\theta}[c[0..d-1]]$にも加算される(Line~\ref{csprob}).

\subsection{色配列による説明可能性}
ユーザ$s$にアイテム$t$を推薦する説明として, パス$s$-$t$の高い確率$P_t^{\theta}[\mathbf{c}]$を持った色配列$\mathbf{c}$を使う. 
ユーザ$u$にアイテム$t$が推薦されるとき, パス$s$-$t$の確率和$P_t^{\theta}$が高いので, $P_t^{\theta}$の高い色配列$\mathbf{c}$を示すことは, $P_t^{\theta}=\sum_{\mathbf{c}}P_t^{\theta}[\mathbf{c}]$であることを考えると, 透明性の観点から合理的な説明である. 
さらに, 色配列は, 推薦するユーザから推薦されたアイテムまでの関係パスであり, 他のオブジェクト間の関係を通じて, ユーザのどのような行動 (視聴や評価, タグ付けなど)がアイテムと強く結びついているかを明らかする. 例えば, 推薦された映画が, ユーザが過去に視聴した映画と同じジャンルの映画であることやユーザが視聴した映画と同じ映画を他のユーザが視聴した映画であることなどが, 色配列よって明らかになる.

\section{実験}
実世界の2つのデータセットを用いて, 提案手法APBRecの推薦精度を最先端の手法と比較する実験を行った. また, 高確率の色配列が推薦説明として有効であることを確認した.

\subsection{実験設定}
本実験では, Cal\_Path\_Probで使用している閾値$\theta$を$10^{-7}$に設定した. その他の設定については後述する.

\subsubsection{データセット}
実験に使用するデータセットは, 映画とレシピのレビューサイトであるMovielens 20M\cite{movielens}とFood.com Recipes and Interactions\footnote{https://www.kaggle.com/shuyangli94/food-com-recipes-and-user-interactions}である. 
どちらのデータセットも, 評価とタグ付けのデータで構成されており, その統計量はTable~\ref{tab:movielens}に示されている.
MovielensとFood.com\footnote{評価値の範囲は0-5で, 評価数が20以下のユーザーも含まれている. 同じ重み関数を使うために尺度を1つずらし, 20レシピ未満の評価をしたユーザを削除した.}の評価値の範囲はそれぞれ1-5と1-6で, 両データセットのすべてのユーザは少なくとも20の評価を持っている. 
データセットには, ユーザが映画やレシピに付けた言葉であるタグが含まれている. 評価データをランダムに分け, 60$\%$の評価をトレーニング用に, 残りの40$\%$をテスト用とした. 以上のようなランダムな分割を5回行った際の平均を報告する.

\begin{table}[tb]
  \caption{データセットの統計量}
  \centering
  \label{tab:movielens}
  \scalebox{0.7}{
  \begin{tabular}{lccccc}
    \toprule
    Dataset &$\#$users&$\#$items&$\#$tags&$\#$ratings&$\#$taggings\\
    \midrule
    Movielens 20M & 138493 & 27278 & 38644 & 20000263 & 465564\\
    Food.com Recipes and Interactions & 6389 & 197317 & 532 & 719548 & 2798545\\ 
  \bottomrule
\end{tabular}
}
\end{table}

\subsubsection{グラフ構築}
2つのデータセットから有向グラフ$G(V,E,p)$を構築する. 頂点集合$V$は, すべてのオブジェクト, つまり, ユーザとアイテム, タグの集合である. 
ここでは, ユーザ$u$, アイテム$m$, タグ$t$が対応する頂点そのものを表すとする. 辺集合$E$は評価情報とタグ情報から以下のように構成される. 
ユーザ$u$によるアイテム$m$の評価ごとに, 2つの色付きの有向辺$(u,m,\mathbf{R})$と$(m,u,\mathbf{r})$が作られる. ユーザ$u$がアイテム$m$にタグ$t$を付けるごとに, 6つの色付きの有向辺$(u,m,\mathbf{T})$と$(m,u,\mathbf{t})$, $(u,t,\mathbf{U})$, $(t,u,\mathbf{u})$, $(m,t,\mathbf{M})$, $(t,m,\mathbf{m})$が作られる.
すると, 色の集合は $C=\{\mathbf{R},\mathbf{r},\mathbf{T},\mathbf{t},\mathbf{U},\mathbf{u},\mathbf{M},\mathbf{m}\}$となり, それぞれの色が表す関係はTable~\ref{tab:cedge} に示す.

\iffalse
\begin{table}[tb]
  \caption{$G(V,E,p)$の色付きの辺は, データセットとその重みを決めるための関連情報, および関係性を表す. また, $r_{u,m}$をユーザー$u$によるアイテム$m$の評価値, $t_{u,m}$をユーザ$u$がタグ付けしたアイテム$m$のタグの集合を表すとする. なお, 集合`$\cdot$'の表記$|\cdot|$は, 集合`$\cdot$'の要素数を意味する.}\label{tab:cedge}
  {\small
  \scalebox{0.8}{
  \begin{tabular}{@{}ccp{4.5cm}|ccl@{}}
    \toprule
    edge  & rel. inf.   & relation & edge & rel. inf. & relation\\
    \midrule
$(u,m,\mathbf{R})$ & $r_{u,m}$ & user $u$ rated item $m$ at $r_{u,m}$. & $(u,t,\mathbf{U})$ & $|\{m \mid t\in t_{u,m}\}|$ &  user $u$ tagged with tag $t$.\\
$(m,u,\mathbf{r})$ & $r_{u,m}$ & item $m$ was rated at $r_{u,m}$ by user $u$. & $(t,u,\mathbf{u})$ & $|\{m \mid t\in t_{u,m}\}|$ & with tag $t$ user $u$ tagged.\\
    $(u,m,\mathbf{T})$ & $|t_{u,m}|$ & user $u$ tagged item $m$. & $(m,t,\mathbf{M})$ & $|\{u \mid t\in t_{u,m}\}|$ & item $m$ was tagged with tag $t$.\\
$(m,u,\mathbf{t})$ & $|t_{u,m}|$ & item $m$ was tagged by user $u$. & $(t,m,\mathbf{m})$ & $|\{u \mid t\in t_{u,m}\}|$ & with tag $t$ item $m$ was tagged.\\
    \bottomrule
\end{tabular}
}
}
\end{table}
\fi

\begin{table}[tb]
  \caption{$G(V,E,p)$の色付きの辺は, データセットとその重みを決めるための関連情報(rel. inf.), および関係性 (relation)を表す. また, $r_{u,m}$をユーザー$u$によるアイテム$m$の評価値, $t_{u,m}$をユーザ$u$がタグ付けしたアイテム$m$のタグの集合を表すとする. なお, 集合`$\cdot$'の表記$|\cdot|$は, 集合`$\cdot$'の要素数を意味する.}\label{tab:cedge}
  {\small
   \scalebox{0.9}{
  \begin{tabular}{@{}ccp{4.5cm}}
    \toprule
    edge  & rel. inf.   & relation\\
    \midrule
$(u,m,\mathbf{R})$ & $r_{u,m}$ & ユーザ$u$がアイテム$m$に評価値$r_{u,m}$を付けた.\\
$(m,u,\mathbf{r})$ & $r_{u,m}$ & アイテム$m$がユーザ$u$に評価値$r_{u,m}$を付けられた.\\
$(u,m,\mathbf{T})$ & $|t_{u,m}|$ & ユーザ$u$がアイテム$m$にタグ付けした.\\
$(m,u,\mathbf{t})$ & $|t_{u,m}|$ & アイテム$m$がユーザ$u$にタグ付けされた.\\
$(u,t,\mathbf{U})$ & $|\{m \mid t\in t_{u,m}\}|$ &  ユーザ$u$がタグ$t$を付けた.\\
$(t,u,\mathbf{u})$ & $|\{m \mid t\in t_{u,m}\}|$ & タグ$t$がユーザ$u$に付けられた. \\
$(m,t,\mathbf{M})$ & $|\{u \mid t\in t_{u,m}\}|$ & アイテム$m$がタグ$t$を付けられた. \\
$(t,m,\mathbf{m})$ & $|\{u \mid t\in t_{u,m}\}|$ & タグ$t$がアイテム$m$に付いた. \\
    \bottomrule
\end{tabular}
}
}
\end{table}

各色$c$に対する関数$w_c$を構築するために, \cite{MRH}で用いられたものと同じ正規化を評価値に適用し, 以下のように定義する. 
ここで, $w_{v_1,v_2,c}$をTable~\ref{tab:cedge}の`rel. inf.'欄に示された辺$(v_1,v_2,c)$の関連情報とする. そして, 色$c\in C$に対する関数$w_c:E\rightarrow (0,\infty)$は次のように定義される.
\[
w_c((v_1,v_2,c))= \frac{w'((v_1,v_2,c))}{\mathrm{ave}(w'((v_1,\cdot,c)))},
\]
ここで, $\mathrm{ave}(w'((v_1,\cdot,c)))$は$\{v\mid (v_1,v,c)\in E\}$上での$w'((v_1,v,c))$の平均であり, 関数$w'$は以下のように定義する. 

\iffalse
\[
w'((v_1,v_2,c))=w'((v_2,v_1,\bar{c}))=\frac{w_{v_1,v_2,c}}{\sqrt{\displaystyle\sum_{v_2':(v_1,v_2',c)\in E}w_{v_1,v_2',c}}\sqrt{\displaystyle\sum_{v_1':(v_2,v_1',\bar{c})\in E}w_{v_1',v_2,\bar{c}}}},
\]
\fi

\begin{eqnarray}
w'((v_1,v_2,c))&=&w'((v_2,v_1,\bar{c})) \nonumber \\ &=&\frac{w_{v_1,v_2,c}}{\sqrt{\displaystyle\sum_{v_2':(v_1,v_2',c)\in E}w_{v_1,v_2',c}}\sqrt{\displaystyle\sum_{v_1':(v_2,v_1',\bar{c})\in E}w_{v_1',v_2,\bar{c}}}},\nonumber
\end{eqnarray}
ここで, $\bar{c}$は色$c$で表される関係の逆の関係を表す色であり, $\bar{\mathbf{R}}=\mathbf{r}$, $\bar{\mathbf{T}}=\mathbf{t}$, $\bar{\mathbf{U}}=\mathbf{u}$ and $\bar{\mathbf{M}}=\mathbf{m}$となる. 
辺の重み関数$p$は, $\sum_{(v,c):(v_1,v,c)\in E}p((v_1,v,c))=1$を満たすように$w_c$を正規化したものである。
\[
p((v_1,v_2,c))=\frac{w_c((v_1,v_2,c))}{\sum_{(v,c):(v_1,v,c)\in E}w_c((v_1,v,c))}.
\]

\subsubsection{評価指標}
推薦精度は, DCG (normalized Discounted Cumulative Gain)\cite{Burges2005}とPrecision, Recall, F1-measureで評価する. ユーザ$u$への推薦アイテムランキング$m_1,m_2,\cdots$のDCG (Discounted Cumulative Gain)は, 下位に向かってスコアを割り引いていくランキング指標である.
\[
DCG@n=\sum_{i=1}^n\frac{2^{r_{u,m_i}}-1}{\log_2(i+1)},
\]
ここで, $r_{u,m_i}$はユーザ$u$に対する推薦ランキングの$i$番目のアイテム$m_i$の評価値であり, $n$は評価に使用する最大ランクである. nDCGはDCGをIDCG (Ideal DCG)で割った正規化されたDCGであり, IDCGはアイテム$m$のランキングを$r_{u,m}$の降順でソートした場合のDCGである.
PrecisionとRecall, F1-measureを用いて高評価アイテムの推薦精度を測定した. 高評価アイテムとは, Movielensで4以上, Food.comで5以上の評価を受けたアイテムと定義した.
ここで, $M^{\mathrm{high}}_u$は, ユーザ$u$によって高く評価されたアイテムの集合を表し, $M^{\mathrm{rec}}_u(n)$は, 上位n個の推薦アイテムの集合とする. また, ユーザ$u$の$\mathrm{Precision@}n$と$\mathrm{Recall@}n$, $\mathrm{F1@}n$は次のように定義される.
これらは0-1の値をとり, 1に近いほど精度は良い. 

\iffalse
\begin{align*}
\mathrm{Precision@}n=&\frac{|M^{\mathrm{high}}_u\cap M^{\mathrm{rec}}_u(n)|}{n},\ \ 
\mathrm{Recall@}n=\frac{|M^{\mathrm{high}}_u\cap M^{\mathrm{rec}}_u(n)|}{|M^{\mathrm{high}}_u|}, \text{ and }\\
\mathrm{F1@}n=&\frac{2\mathrm{Precision@}n\cdot \mathrm{Recall@}n}{\mathrm{Precision@}n+\mathrm{Recall@}n}.
\end{align*}
\fi

\[
\mathrm{Precision@}n=\frac{|M^{\mathrm{high}}_u\cap M^{\mathrm{rec}}_u(n)|}{n},
\]
\[
\mathrm{Recall@}n=\frac{|M^{\mathrm{high}}_u\cap M^{\mathrm{rec}}_u(n)|}{|M^{\mathrm{high}}_u|},
\]
\[
\mathrm{F1@}n=\frac{2\mathrm{Precision@}n\cdot \mathrm{Recall@}n}{\mathrm{Precision@}n+\mathrm{Recall@}n}.
\]

\subsubsection{比較手法}
提案手法の精度を, ベースラインモデルおよび最先端モデルの精度と比較した. AVEとIBCF, MRH, BPRの4つのモデルを用いた. 
AVEはベースラインモデルであり, アイテムを評価したすべてのユーザの評価を平均した評価値によってアイテムをランク付けする. IBCFモデルは, アイテムベースの協調フィルタリングモデル\cite{sarwar2001item}で, 評価$r_{u,m}$を次の式で予測する.
\[
\frac{\sum_{m'\in M_u} \mathrm{cosine}(r_{\cdot,m},r_{\cdot,m'})r_{u,m}}{\sum_{m'\in M_u} \mathrm{cosine}(r_{\cdot,m},r_{\cdot,m'})},
\]
ここで, 
\[
\mathrm{cosine}(r_{\cdot,m},r_{\cdot,m'})=\frac{\sum_{u\in U_m\cap U_{m'}}r_{u,m}r_{u,m'}}{\sqrt{\sum_{u\in U_m\cap U_{m'}}r_{u,m}^2}\sqrt{\sum_{u\in U_m\cap U_{m'}}r_{u,m'}^2}},
\]
$M_u$はユーザ$u$が評価した映画の集合, $U_m$はアイテム$m$を評価したユーザの集合である. 
BPR (Bayesian Personalized Ranking)\cite{rendle2012bpr}は, 行列分解の一種であり, 各ユーザのアイテムペアを正しくランク付けする事後確率を最適化する. http://ethen8181.github.io/machine-learning/recsys/4\_bpr.htmlに掲載されている実装を使用している. パラメータはlearning\_rate=0.1, n\_iters=3000, n\_factors=60, batch\_size=100 以外はデフォルトの値である. 
MRH (Music Recommendation via Hypergraph)\cite{MRH}は, ソーシャル情報を利用したグラフベースの推薦手法である. MRHモデルのRandom Walks with Restarts版において, 評価情報とタグ情報を用いてハイパーグラフを構築し, 遷移行列$A$を作成する. ユーザ$u$への推薦には, $\mathbf{f}^{(t+1)}=\alpha A\mathbf{f}^{(t)}+(1-\alpha)\mathbf{y}_u$に対する$\lim_{t\rightarrow \infty}\mathbf{f}^{(t)}$の定常分布を用いる. ここで, $\mathbf{y}_u$はユーザ$u$に対するクエリベクトルであり, 0以外の要素は$u$に対応する要素を$1$に設定し, $u$とグラフ上でつながっている$v$に対応する要素を$A_{u,v}$に設定する. 実験では, リスタート確率$1-\alpha$を$0.04$とし, $t=80$のときの定常分布を$\mathbf{f}^{(t)}$で近似している. 

\subsection{結果}
\subsubsection{推薦精度}
全ユーザの結果を平均したNDCG@10と, Precision@10, Recall@10, F1@10を5回の95$\%$信頼区間付きでTable~\ref{tab:expresult}に示す. APBRecは, ベースライン手法であるAVEを上回り, 他の3つの説明のない最新の推薦手法と同等の性能を有している.

\begin{table*}[tb]
  \caption{2つのデータセットに対する5つの推薦モデルの推薦精度をnDCG@10と, Precision@10, Recall@10, F1@10で表したもの. 数値は全ユーザの5回の実行結果を平均したもので, 95\%信頼区間を括弧内に示している. 最も高い値は太字で, それと統計的に有意な差がないものは斜体で示した.}
  \centering
  \label{tab:expresult}
  \begin{tabular}{llllll}
    \toprule
    dataset&method& nDCG & Precision & Recall & F1\\
    \midrule
    \multirow{5}{*}{Movielens}
    &AVE& 0.4508$\ci{0.0003}$ &0.3658$\ci{0.0006}$ &0.3625$\ci{0.0002}$ & 0.3106$\ci{0.0004}$\\
    &IBCF& \textbf{0.7667}$\ci{0.0001}$& \textbf{0.6901}$\ci{0.0005}$&\textbf{0.5239}$\ci{0.0005}$&\textbf{0.4926}$\ci{0.0004}$\\
    &MRH& 0.7354$\ci{0.0003}$ &0.6478$\ci{0.0004}$ &0.4991$\ci{0.0005}$ &0.4657$\ci{0.0004}$  \\
    &BPR& 0.6885 $\ci{0.0047}$& 0.6123 $\ci{0.0026}$& 0.4770$\ci{0.0016}$ & 0.4419$\ci{0.0018}$ \\
    &APBRec& 0.7023 $\ci{0.0002}$&0.6241 $\ci{0.0004}$&0.4799 $\ci{0.0003}$&0.4474 $\ci{0.0003}$ \\
    \midrule
    \multirow{5}{*}{Food.com}
    &AVE&0.8328$\ci{0.0011}$ & 0.8587$\ci{0.0014}$&0.5673$\ci{0.0018}$&0.6094$\ci{0.0017}$\\
    &IBCF& 0.8745$\ci{0.0012}$ & \textit{0.8785}$\ci{0.0018}$ & \textit{0.5755}$\ci{0.0016}$ & \textit{0.6199}$\ci{0.0018}$\\
    &MRH& 0.8659$\ci{0.0012}$&0.8743 $\ci{0.0021}$&\textit{0.5740}$\ci{0.0014}$ & \textit{0.6179}$\ci{0.0020}$\\
    &BPR&\textbf{0.8804}$\ci{0.0008}$ & \textbf{0.8806}$\ci{0.0018}$& \textbf{0.5761}$\ci{0.0011}$&\textbf{0.6208}$\ci{0.0015}$\\
    &APBRec&0.8735$\ci{0.0009}$ &0.\textit{8777}$\ci{0.0019}$ & \textit{0.5752}$\ci{0.0013}$&\textit{0.6196}$\ci{0.0018}$\\
  \bottomrule
\end{tabular}
\end{table*}

\subsubsection{説明可能性}
データFood.comに対するAPBRecによる2人のユーザのトップ10の推薦リストをTable~\ref{tab:reclist}に示す. 
各推薦レシピの説明として, 最も確率の高い3つの色配列が示されている. この色配列は, 推奨レシピの推薦理由を示し, 推薦をより透明化させる. 
例えば, 色配列``$\mathbf{RrR}$''は, 「あなたが高評価したレシピを高評価した他のユーザが高評価したレシピ」と解釈することができる. 
また, 推薦リストに対するタグの貢献度を見ることができる. 色配列``$\mathbf{Um}$''は, ユーザー231に推薦された5つのレシピの中で、最も順位の高い色配列である. 
また, ``$\mathbf{Um}$''の解釈としては, 「自分が付けたタグと同じタグが付けられたレシピ」とできる.

\begin{table*}[tb]
  \caption{Food.comにおいてAPBRecがユーザ133と231に推薦したレシピのトップ10. また, 各推薦レシピについて, 最も確率の高い3つの色配列を示している.}
  \label{tab:reclist}
   \scalebox{0.9}{
  \begin{tabular}{l|rllll}
    \toprule
    \multirow{2}{*}{user}&\multirow{2}{*}{rank} &\multirow{2}{*}{recipe}& \multicolumn{3}{c}{color sequence ranking}\\
    \cline{4-6}
    &&&1st$(\%)$&2nd$(\%)$&3rd$(\%)$\\
    \midrule
    \multirow{10}{*}{133}&1& javanese roasted salmon and wilted spinach& $\mathbf{RrR}(92.3)$ & $\mathbf{TrR}(7.6)$ & \\
    &2 &beer& $\mathbf{RrR}(91.6)$ & $\mathbf{TrR}(8.3)$ & \\ 
    &3&chilli beer damper& $\mathbf{RrR}(94.2)$ & $\mathbf{TrR}(5.7)$ & \\
    &4&meat pinwheels& $\mathbf{TrR}(68.4)$ & $\mathbf{RtR}(31.5)$ &\\
    &5&spicy calamari with bacon and scallions& $\mathbf{RrR}(86.5)$ & $\mathbf{TrR}(12.3)$&$\mathbf{Um}(1.0)$ \\
    &6&roasted green beans with garlic and pine nuts&$\mathbf{RrR}(91.7)$ & $\mathbf{TrR}(5.6)$&$\mathbf{RMm}(2.5)$ \\
    &7&bubble and squeak& $\mathbf{RrR}(92.5)$ & $\mathbf{TrR}(7.4)$ & \\
    &8&tamarind sauce& $\mathbf{TrR}(100)$ &  & \\
    &9&citrus morning sunrise& $\mathbf{TrR}(96.5)$ & $\mathbf{RrR}(3.4)$& \\
    &10&maple sugar pumpkin pie& $\mathbf{TmM}(68.7)$ & $\mathbf{Um}(31.2)$&\\
    \midrule
    \multirow{10}{*}{231}&1& baked sausage stuffed jumbo pasta shells& $\mathbf{Um}(63.2)$ & $\mathbf{TMm}(33.1)$ &$\mathbf{Uut}(1.2)$ \\
    &2 &greek spinach rice balls& $\mathbf{RrR}(100)$ &   \\ 
    &3&raspberry wine toaster bread& $\mathbf{RrR}(100)$ &  \\
    &4&crunchy poppy seed chicken salad& $\mathbf{RrR}(100)$ &  \\
    &5&salt rising bread& $\mathbf{RrR}(100)$ &  \\
    &6&garlic lime chicken&$\mathbf{Um}(62.8)$ & $\mathbf{TMm}(32.8)$ &$\mathbf{RMm}(3.1)$  \\
    &7&the best mexican tortilla roll ups& $\mathbf{RrR}(93.7)$ & $\mathbf{TrR}(5.3)$ &$\mathbf{Um}(0.9)$ \\
    &8&quick fish stew&$\mathbf{Um}(62.8)$ & $\mathbf{TMm}(32.8)$ &$\mathbf{RMm}(3.1)$  \\
    &9&mustard herb flank steak& $\mathbf{Um}(63.0)$ & $\mathbf{TMm}(32.9)$ &$\mathbf{RMm}(3.2)$  \\
    &10&caramel apple cupcakes& $\mathbf{Um}(63.5)$ & $\mathbf{TMm}(33.2)$ &$\mathbf{RMm}(3.2)$\\
    \bottomrule 
\end{tabular}
}
\end{table*}

最高確率の色配列が異なると, 主な推奨理由も異なる. したがって, 最高確率の色配列で推薦アイテムを分けることで, 異なるタイプの推薦リストが得られることが期待できる. Table~\ref{tab:tag-relatedcolor}のAPBRecによるデータMovielensのユーザ1741に対する推薦リストは, この予想を裏付けるものである. 
最初のリストともう一つのリストは, アイテムの最高確率の色配列が$\mathbf{RrR}$であるかどうかによって分けられている. 
最初のリストには, Sci-FiやFilm-Noir, Mystery, Warというジャンルが, もう一つのリストにはChildrenやMusical, Horror, Action, Adventure, IMAXというジャンルが含まれており, 2つのリストのジャンル分布は異なっている.

\begin{table*}[tb]
  \caption{Movielensにおいて, APBRecがユーザ1741に推薦した映画のトップ30. 最初のリストともう一つのリストは, そのアイテムの最高確率の色配列が$\mathbf{RrR}$であるかどうかによって分けられている. 斜体のジャンルは, 他のリストに出現しないものを示す.}
  \label{tab:tag-relatedcolor}
  \scalebox{0.8}{
  \begin{tabular}{rlp{6.6cm}lll}
    \toprule
    %\multirow{2}{*}{rank}
    \multicolumn{2}{l}{\multirow{2}{*}{rank movie}}& \multirow{2}{*}{genres}&\multicolumn{3}{c}{color sequence ranking}\\
    \cline{4-6}
    &&&1st&2nd&3rd\\
    \midrule
     1&The Man Who Wouldn't Die&Crime,Drama,Thriller& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     2&Slam Dance&Thriller& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     3&Shepherd&\textit{Sci-Fi}& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     9&Dragon Age&Animation,Fantasy& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     16&Lady on a Train&Comedy,Crime,\textit{Film-Noir},\textit{Mystery},Romance, Thriller&$\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     17&Long Night's Journey Into Day&Documentary& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     20&Jim Jefferies Fully Functional&Comedy& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     22&Napoleon&Drama,\textit{War}& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     26&Big Parade&Drama,Romance,\textit{War}& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
     27&Fawlty Towers&Comedy& $\mathbf{RrR}$&$\mathbf{TrR}$&$\mathbf{UmrR}$\\
    \midrule
      4& Momo& Animation,\textit{Children},Fantasy& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuR}$\\
      5& Houseboat& Comedy,Romance& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      6& Pina& Documentary,\textit{Musical}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuR}$\\
      7& Alvin and the Chipmunks& \textit{Children},Comedy& $\mathbf{Um}$ & $\mathbf{RMm}$&$\mathbf{TrR}$\\
      8& The Wolf Brigade& Animation,Fantasy,Thriller& $\mathbf{Um}$
      &$\mathbf{RrR}$&$\mathbf{TrR}$\\
      10& My Name Is Bruce& Comedy,\textit{Horror}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      11& Party Monster& Comedy,Crime,Drama,Thriller& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      12& Out of Africa& Drama,Romance& $\mathbf{Um}$ & $\mathbf{RrR}$&$\mathbf{TtR}$\\
      13& Shattered Glass& Crime,Drama& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      14& Bat& \textit{Horror}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuR}$\\
      15& Children of the Corn& \textit{Horror}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UmrR}$\\
      18& V.I.P.s& Drama& $\mathbf{TrR}$ & $\mathbf{UmrR}$&$\mathbf{TrRrR}$\\
      19& A Question of Faith& Drama& $\mathbf{Um}$ & $\mathbf{UmMm}$&$\mathbf{TMm}$\\
      21& Double Dynamite& Comedy,\textit{Musical}& $\mathbf{TrR}$ & $\mathbf{UmrR}$&\\
      23& Big Fella& Drama,\textit{Musical}& $\mathbf{TrR}$ & $\mathbf{UmrR}$&\\\
      24& The Guardian& \textit{Action},Thriller& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UmrR}$\\
      25& The Killer Whale& \textit{Action},Drama,\textit{Horror},Thriller& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{RrR}$\\
      28& Bella& Drama,Romance& $\mathbf{Um}$ & $\mathbf{RrR}$&$\mathbf{TrR}$\\
      29& How to Train Your Dragon& \textit{Adventure},Animation,\textit{Children},Fantasy,\textit{IMAX}& $\mathbf{Um}$ & $\mathbf{TrR}$&$\mathbf{UuUm}$\\
      30& Art and Craft& Documentary& $\mathbf{Um}$ & $\mathbf{UuR}$&$\mathbf{TMm}$\\
    \bottomrule
\end{tabular}
}
\end{table*}

\section{おわりに}
本稿では, 高確率の色配列 (高寄与の関係パス)を推薦の説明として示す推薦アルゴリズムを提案した. 
本アルゴリズムでは, 高確率の色配列を計算するために, 推薦に関連するオブジェクト間の様々な関係を表す有向グラフ上を深さ優先探索を行った. 
閾値以上の確率を持つすべての非巡回パス$s$-$t$の確率合計が, ユーザ$s$に対するアイテム$t$の推薦スコアに用いられる. 
実世界のデータセットを用いた実験によると, 提案手法の推薦精度は, 説明不可能な最新の推薦手法に匹敵する. 
また, 色配列は, 推薦の説明だけでなく, 様々な傾向の推薦リストを実現するためにも有用であることが確認された.

%\vspace{30mm} <- 文献が本文と近すぎるときは適宜利用してください．
\vspace{2em}
\bibliography{mybibliography}


\end{document}
%
%
% End of file: sample.tex
