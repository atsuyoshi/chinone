//
//  recommend.cpp
//

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include "timer.hpp"
#include "hash.hpp"
#include "graph.hpp"

using namespace std;

char buffer[256];
ifstream fin;

int rating_num=0;
int user_num=0;
int movie_num=0;
hashing *taginfo;
int node_num;

int *user_edge_num=NULL;
int *movie_edge_num=NULL;
int *tag_edge_num=NULL;

void max_id_rating(char *file_name) {
  fin.open(file_name);
  fin.getline(buffer,sizeof(buffer)); //skip header 
  while(fin.getline(buffer,sizeof(buffer))) {
    int user_id,movie_id;
    float rating;
    //    userId,movieId,rating,timestamp
    sscanf(buffer,"%d,%d,%f",&user_id,&movie_id,&rating);
    if(user_id>user_num) user_num=user_id;
    if(movie_id>movie_num) movie_num=movie_id;
    rating_num++;
  }
  fin.close();
}

void max_id_tag(char *file_name) {
  // count lines in tag_file
  int line_num=0;
  fin.open(file_name);
  fin.getline(buffer,sizeof(buffer)); //skip header 
  while(fin.getline(buffer,sizeof(buffer))) {
    line_num++;
  }
  fin.close();

  cout<<"#line_in_"<<file_name<<"="<<line_num<<endl;

  taginfo=new hashing(line_num*2);
  
  fin.open(file_name);
  fin.getline(buffer,sizeof(buffer)); //skip header 
  while(fin.getline(buffer,sizeof(buffer))) {
    int user_id,movie_id;
    char tag[256];
    //    userId,movieId,rating,timestamp
    sscanf(buffer,"%d,%d,%s",&user_id,&movie_id,tag);
    if(user_id>user_num) user_num=user_id;
    if(movie_id>movie_num) movie_num=movie_id;
    taginfo->hash_insert(tag);
  }
  fin.close();
}


void count_rating(char *file_name) {
  fin.open(file_name);
  fin.getline(buffer,sizeof(buffer)); //skip header 
  while(fin.getline(buffer,sizeof(buffer))) {
    int user_id,movie_id;
    float rating;
    //    userId,movieId,rating,timestamp
    sscanf(buffer,"%d,%d,%f",&user_id,&movie_id,&rating);
    user_edge_num[user_id-1]++;
    movie_edge_num[movie_id-1]++;
  }
  fin.close();
}

void count_tag(char *file_name) {  
  fin.open(file_name);
  fin.getline(buffer,sizeof(buffer)); //skip header 
  while(fin.getline(buffer,sizeof(buffer))) {
    int user_id,movie_id,tag_id;
    char tag[256];
    //    userId,movieId,rating,timestamp
    sscanf(buffer,"%d,%d,%s",&user_id,&movie_id,tag);
    tag_id=taginfo->hash_find(tag);
    user_edge_num[user_id-1]+=2;
    movie_edge_num[movie_id-1]+=2;
    tag_edge_num[tag_id]+=2;
  }
  fin.close();  
}

void load_rating_weight(char *file_name, graph *graph) {
  fin.open(file_name);
  fin.getline(buffer,sizeof(buffer)); //skip header 
  while(fin.getline(buffer,sizeof(buffer))) {
    int user_id,movie_id;
    float rating;
    //    userId,movieId,rating,timestamp
    sscanf(buffer,"%d,%d,%f",&user_id,&movie_id,&rating);
    user_id--;
    movie_id+=user_num-1;
    graph->adjacent_list[user_id][graph->edge_pos[0][user_id]].to=movie_id;
    graph->adjacent_list[user_id][graph->edge_pos[0][user_id]].weight=rating;
    graph->edge_pos[0][user_id]++;
    graph->adjacent_list[movie_id][graph->edge_pos[0][movie_id]].to=user_id;
    graph->adjacent_list[movie_id][graph->edge_pos[0][movie_id]].weight=rating;
    graph->edge_pos[0][movie_id]++;
  }
  fin.close();
  for(int i=0;i<user_num+movie_num;i++) graph->edge_pos[1][i]=graph->edge_pos[0][i];
}

void load_tag_weight(char *file_name, graph *graph) {
  fin.open(file_name);
  fin.getline(buffer,sizeof(buffer)); //skip header 
  while(fin.getline(buffer,sizeof(buffer))) {
    int user_id,movie_id,tag_id;
    char tag[256];
    //    userId,movieId,rating,timestamp
    sscanf(buffer,"%d,%d,%s",&user_id,&movie_id,tag);
    tag_id=taginfo->hash_find(tag)+user_num+movie_num;
    user_id--;
    movie_id+=user_num-1;
    graph->adjacent_list[user_id][graph->edge_pos[1][user_id]].to=movie_id;
    graph->adjacent_list[user_id][graph->edge_pos[1][user_id]].weight=1;
    graph->edge_pos[1][user_id]++;
    graph->adjacent_list[user_id][graph->edge_pos[1][user_id]].to=tag_id;
     graph->adjacent_list[user_id][graph->edge_pos[1][user_id]].weight=1;
    graph->edge_pos[1][user_id]++;
    graph->adjacent_list[movie_id][graph->edge_pos[1][movie_id]].to=user_id;
     graph->adjacent_list[movie_id][graph->edge_pos[1][movie_id]].weight=1;
    graph->edge_pos[1][movie_id]++;
    graph->adjacent_list[movie_id][graph->edge_pos[1][movie_id]].to=tag_id;
     graph->adjacent_list[movie_id][graph->edge_pos[1][movie_id]].weight=1;
    graph->edge_pos[1][movie_id]++;
    graph->adjacent_list[tag_id][graph->edge_pos[1][tag_id]].to=user_id;
     graph->adjacent_list[tag_id][graph->edge_pos[1][tag_id]].weight=1;
    graph->edge_pos[1][tag_id]++;
    graph->adjacent_list[tag_id][graph->edge_pos[1][tag_id]].to=movie_id;
     graph->adjacent_list[tag_id][graph->edge_pos[1][tag_id]].weight=1;
    graph->edge_pos[1][tag_id]++;
  }
  fin.close();
}
/*
void rating_weight_normalization(graph *graph) {
  int um_num=user_num+movie_num;
  float *weight_norm=new float[um_num];
  for(int i=0;i<um_num;i++) weight_norm[i]=0;
  for(int i=0;i<um_num;i++) {
    for(int j=0;j<graph->edge_pos[0][i];j++) {
	weight_norm[i]+=graph->adjacent_list[i][j].weight;
	weight_norm[graph->adjacent_list[i][j].to]+=graph->adjacent_list[i][j].weight;
    }
  }
  for(int i=0;i<um_num;i++) weight_norm[i]=sqrt(weight_norm[i]);
  for(int i=0;i<um_num;i++) {
    float weight_sum=0;
    for(int j=0;j<graph->edge_pos[0][i];j++) {
        graph->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[graph->adjacent_list[i][j].to];
        weight_sum+=graph->adjacent_list[i][j].weight;
     }    
    for(int j=0;j<graph->edge_pos[0][i];j++) graph->adjacent_list[i][j].weight/=weight_sum/graph->edge_pos[0][i];
  }
  delete[] weight_norm;
}
*/
void rating_weight_normalization(graph *graph) {
  int um_num=user_num+movie_num;
  double *weight_norm=new double[um_num];
  for(int i=0;i<um_num;i++) weight_norm[i]=0;
  for(int i=0;i<um_num;i++) {
    for(int j=0;j<graph->edge_pos[0][i];j++) {
	weight_norm[i]+=graph->adjacent_list[i][j].weight;
	/*	weight_norm[G->adjacent_list[i][j].to]+=G->adjacent_list[i][j].weight; */
    }
  }
  for(int i=0;i<um_num;i++) weight_norm[i]=sqrt(weight_norm[i]);
  for(int i=0;i<um_num;i++) {
    float weight_sum=0;
    for(int j=0;j<graph->edge_pos[0][i];j++) {
        graph->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[graph->adjacent_list[i][j].to];
        weight_sum+=graph->adjacent_list[i][j].weight;
     }    
    for(int j=0;j<graph->edge_pos[0][i];j++) graph->adjacent_list[i][j].weight/=weight_sum/graph->edge_pos[0][i];
  }
  delete[] weight_norm;
}

int compare_to(const void *a, const void *b)
{
  return ((Edge*)a)->to - ((Edge*)b)->to;
}

/*
void merge_tag_weight(graph *graph) {
  for(int i=0;i<node_num;i++) {
    int size=graph->edge_pos[1][i]-graph->edge_pos[0][i];
    if(size>1) {
      qsort(&(graph->adjacent_list[i][graph->edge_pos[0][i]]),size,sizeof(Edge),compare_to);
      int cur_pos=graph->edge_pos[0][i];
      for(int j=cur_pos+1;j<graph->edge_pos[1][i];j++) {
	if(graph->adjacent_list[i][j].to==graph->adjacent_list[i][cur_pos].to) graph->adjacent_list[i][cur_pos].weight+=graph->adjacent_list[i][j].weight;
	else {
	  cur_pos++;
	  graph->adjacent_list[i][cur_pos]=graph->adjacent_list[i][j];
	}
      }
      graph->edge_pos[1][i]=cur_pos+1;
    }
  }
}
*/
void merge_tag_weight(graph *graph) {
  for(int i=0;i<node_num;i++) {
    int size=graph->edge_pos[1][i]-graph->edge_pos[0][i];
    if(size>1) {
      qsort(&(graph->adjacent_list[i][graph->edge_pos[0][i]]),size,sizeof(Edge),compare_to);
      int cur_pos=graph->edge_pos[0][i];
      for(int j=cur_pos+1;j<graph->edge_pos[1][i];j++) {
	if(graph->adjacent_list[i][j].to==graph->adjacent_list[i][cur_pos].to) graph->adjacent_list[i][cur_pos].weight+=graph->adjacent_list[i][j].weight;
	else {
	  cur_pos++;
	  graph->adjacent_list[i][cur_pos]=graph->adjacent_list[i][j];
	}
      }
      graph->edge_pos[1][i]=cur_pos+1;
    }
  }
}
/*
void weight_normalization(graph *graph) {
  for(int i=0;i<node_num;i++) {
    float degree=0;
    int j=0;
    for(;j<graph->edge_pos[0][i];j++) {
      degree+=graph->adjacent_list[i][j].weight;
    }
    for(;j<graph->edge_pos[1][i];j++) {
      degree+=graph->adjacent_list[i][j].weight/2;
    }
    j=0;
    for(;j<graph->edge_pos[0][i];j++) {
      graph->adjacent_list[i][j].weight/=2*degree;
    }
    for(;j<graph->edge_pos[1][i];j++) {
      graph->adjacent_list[i][j].weight/=3*degree;
    }
  }
}
*/
void tag_weight_normalization(graph *graph) {
  double *weight_norm=new double[node_num];
  for(int i=0;i<node_num;i++) weight_norm[i]=0;
  for(int i=0;i<node_num;i++) {
    for(int j=graph->edge_pos[0][i];j<graph->edge_pos[1][i];j++) {
	weight_norm[i]+=graph->adjacent_list[i][j].weight;
	/*	weight_norm[G->adjacent_list[i][j].to]+=G->adjacent_list[i][j].weight; */
    }
  }
  for(int i=0;i<node_num;i++) weight_norm[i]=sqrt(weight_norm[i]);
  int um_num=user_num+movie_num;
  for(int i=0;i<um_num;i++) {
    float weight_sum=0;
    int j=graph->edge_pos[0][i];
    for(;j<graph->edge_pos[1][i];j++) {
        if(graph->adjacent_list[i][j].to>=um_num) break;
        graph->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[graph->adjacent_list[i][j].to];
        weight_sum+=graph->adjacent_list[i][j].weight;
    }
    int k=graph->edge_pos[0][i];
    int size=j-k;
    for(;k<j;k++) graph->adjacent_list[i][k].weight/=weight_sum/size;
    weight_sum=0;
    for(;j<graph->edge_pos[1][i];j++) {
        graph->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[graph->adjacent_list[i][j].to];
        weight_sum+=graph->adjacent_list[i][j].weight;
    }
    size=graph->edge_pos[1][i]-k;
    for(;k<graph->edge_pos[1][i];k++) graph->adjacent_list[i][k].weight/=weight_sum/size;
  }
  for(int i=um_num;i<node_num;i++) {
    float weight_sum=0;
    int j=graph->edge_pos[0][i];
    for(;j<graph->edge_pos[1][i];j++) {
        if(graph->adjacent_list[i][j].to>=user_num) break;
        graph->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[graph->adjacent_list[i][j].to];
        weight_sum+=graph->adjacent_list[i][j].weight;
    }
    int k=graph->edge_pos[0][i];
    int size=j-k;
    for(;k<j;k++) graph->adjacent_list[i][k].weight/=weight_sum/size;
    weight_sum=0;
    for(;j<graph->edge_pos[1][i];j++) {
        graph->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[graph->adjacent_list[i][j].to];
        weight_sum+=graph->adjacent_list[i][j].weight;
    }
    size=graph->edge_pos[1][i]-k;
    for(;k<graph->edge_pos[1][i];k++) graph->adjacent_list[i][k].weight/=weight_sum/size;
  }
  delete[] weight_norm;
}

void weight_normalization(graph *graph) {
  for(int i=0;i<node_num;i++) {
    float degree=0;
    for(int j=0;j<graph->edge_pos[1][i];j++) {
      degree+=graph->adjacent_list[i][j].weight;
    }
    for(int j=0;j<graph->edge_pos[1][i];j++) {
      graph->adjacent_list[i][j].weight/=degree;
    }
    /*****************************************
    int j=0;
    for(;j<G->edge_pos[0][i];j++) {
      degree+=G->adjacent_list[i][j].weight;
    }
    for(;j<G->edge_pos[1][i];j++) {
      degree+=G->adjacent_list[i][j].weight/2;
    }
    j=0;
    for(;j<G->edge_pos[0][i];j++) {
      G->adjacent_list[i][j].weight/=2*degree;
    }
    for(;j<G->edge_pos[1][i];j++) {
      G->adjacent_list[i][j].weight/=3*degree;
    }
    *****************************************/
  }
}
int main(int argc,char **argv)
{
  if(argc<6) {
    cerr << "usage:" << argv[0] << " rating_file tag_file user_id rec_num theshold_prob" << endl;
    exit(1);
  }

  cout << "prog="<< argv[0]<<endl;
  cout << "rating_file="<< argv[1]<<endl;
  cout << "tag_file="<< argv[2]<<endl;

  int user_id=atoi(argv[3]);
  int rec_num=atoi(argv[4]);
  float threshold_prob=atof(argv[5]);
  cout << "user_id="<< user_id<<endl;
  cout << "rec_num="<< rec_num<<endl;
  cout << "threshold_prob="<< threshold_prob<<endl;

  
  max_id_rating(argv[1]);
  max_id_tag(argv[2]);  

  cout << "user_num="<< user_num<<endl;
  cout << "movie_num="<< movie_num<<endl;
  cout << "tag_num="<< taginfo->stored_num<<endl;

  node_num=user_num+movie_num+taginfo->stored_num;

  cout << "node_num="<< node_num<<endl;

  int *edge_num=new int[node_num];
  for(int i=0;i<node_num;i++) edge_num[i]=0;
  user_edge_num=edge_num;
  movie_edge_num=&edge_num[user_num];
  tag_edge_num=&movie_edge_num[movie_num];
  
  count_rating(argv[1]);
  count_tag(argv[2]);  
  
  graph  *G=new graph(node_num,edge_num,user_num,movie_num,threshold_prob);
  
  load_rating_weight(argv[1], G);
  load_tag_weight(argv[2],G);
  rating_weight_normalization(G);
  merge_tag_weight(G);
  tag_weight_normalization(G);
  weight_normalization(G);
  RecInfo *rec_list=new RecInfo[rec_num];
  G->recommend(user_id-1,rec_num,rec_list,NULL,0);
  for(int i=0;i<rec_num;i++) {
    cout<<"("<<i<<") "<< rec_list[i].id+1 <<":"<<rec_list[i].prob<<endl;
    for(int j=0;j<PATHSEQNUM;j++) {
      if(rec_list[i].path_seq[j].prob<0) break;
      cout<<"["<<rec_list[i].path_seq[j].type_seq<<","<<rec_list[i].path_seq[j].prob<<","<<rec_list[i].path_seq[j].count<<","<< 100 * rec_list[i].path_seq[j].prob /rec_list[i].prob<<"%]";
    }
    cout<<endl;
  }
#ifdef TIME
  timer::print_cpu_time();
#endif

}
