//
//  strbuffer.hpp
//
#ifndef _INC_STRBUFFER
#define _INC_STRBUFFER

class strbuffer {
  int size;
  char *buffer;
  int tail;
public:
  strbuffer(int in_size);
  void clear();
  char *store(char *str);
};
#endif    //INC_STRBUFFER
