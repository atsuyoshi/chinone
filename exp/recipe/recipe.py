import sys

from numpy.core.defchararray import index
def sample(i,j,k,l,m,n,o,p,q,r):
    import pandas as pd
    import numpy as np
    from scipy import sparse
    import math
    from sklearn.model_selection import KFold
    import csv
    from cppRelationPath import RelationPath

    #データの取得
    df_ratings = pd.read_csv('food/RAW_interactions.csv',dtype={'rating':float})
    dR = df_ratings.loc[:,['user_id','recipe_id','rating']]

    #評価数20以上に制限
    dRR = dR.groupby('user_id').count()['recipe_id'] > 20
    dRR = pd.DataFrame(dRR)
    dRR = dRR.rename(columns={'recipe_id': 'flag'})
    dRR = dRR.reset_index()
    dRR = pd.merge(dR,dRR,how='left')
    dRR = dRR[dRR['flag'] == True]
    dRR = dRR.drop('flag', axis=1)
    dR = dRR


    #user_idの振り直し
    tmp = dR.drop_duplicates(subset='user_id').copy()
    lu = len(tmp)
    tmp['user_ID'] = range(lu)
    tmp3 = tmp.loc[:,['user_id','user_ID']]
    tmp = pd.merge(dR, tmp,on='user_id',how='left')
    tmp = tmp.drop(['recipe_id_y', 'rating_y'], axis=1)
    tmp = tmp.rename(columns={'recipe_id_x': 'recipe_id', 'rating_x': 'rating'})

    #recipe_idの振り直し
    tmp2 = tmp.drop_duplicates(subset='recipe_id').copy()
    lm = len(tmp2)
    tmp2['recipe_ID'] = range(lm)
    tmp2 = tmp2.loc[:,['recipe_id','recipe_ID']]
    tmp = pd.merge(tmp, tmp2,on='recipe_id',how='left')
    
    dR = tmp.loc[:,['user_ID','recipe_ID','rating']]

    #タグ
    df_tags = pd.read_csv('food/RAW_recipes.csv')
    dT = df_tags.loc[:, ['id','contributor_id','tags','name']]
    #タグだけついていて、評価されていないレシピがあるからNANになってしまう
    dT = pd.merge(dT, tmp2,how='left',left_on='id',right_on='recipe_id').drop(columns=['id','recipe_id'])
    
    #tagging is 231637
    dT = dT.dropna()
    #tagging is 197857   
    #タグのみ作り評価をしていないユーザがいるからNANになる
    dT = pd.merge(dT, tmp3,how='left',left_on='contributor_id',right_on='user_id').drop(columns=['contributor_id','user_id'])
    #タグのみ作り評価をしていないデータを削除,row_num is213598
    dT = dT.dropna()
    
    #tagging is 153684
    dT['user_ID'] = dT['user_ID'].astype('int')
    dT['recipe_ID'] = dT['recipe_ID'].astype('int')

    print(dT[dT['recipe_ID'] == i]['name'])
    print(dT[dT['recipe_ID'] == j]['name'])
    print(dT[dT['recipe_ID'] == k]['name'])
    print(dT[dT['recipe_ID'] == l]['name'])
    print(dT[dT['recipe_ID'] == m]['name'])
    print(dT[dT['recipe_ID'] == n]['name'])
    print(dT[dT['recipe_ID'] == o]['name'])
    print(dT[dT['recipe_ID'] == p]['name'])
    print(dT[dT['recipe_ID'] == q]['name'])
    print(dT[dT['recipe_ID'] == r]['name'])


    return


if __name__ == '__main__':
    args = sys.argv
    i = int(args[1])
    j = int(args[2])
    k = int(args[3])
    l = int(args[4])
    m = int(args[5])
    n = int(args[6])
    o = int(args[7])
    p = int(args[8])
    q = int(args[9])
    r = int(args[10])
    sample(i,j,k,l,m,n,o,p,q,r)
