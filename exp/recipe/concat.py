import pandas as pd
import numpy as np
"""
df = pd.read_csv('./result/ndcg_0_user0.csv',header=None,skip_blank_lines=False)

for i in range(1,5):
    df_tmp = pd.read_csv('./result/ndcg_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    df = pd.concat([df,df_tmp],axis=1)

df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]

ndcg = df["sum"]
d = pd.DataFrame(ndcg)
d /= 5
d.to_csv("./result/ndcg.csv",header=0,index=False)

df = pd.read_csv('./result/precision_0_user0.csv',header=None,skip_blank_lines=False)

for i in range(1,5):
    df_tmp = pd.read_csv('./result/precision_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    df = pd.concat([df,df_tmp],axis=1)

df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]

ndcg = df["sum"]
d = pd.DataFrame(ndcg)
d /= 5
d.to_csv("./result/precision.csv",header=0,index=False)

df = pd.read_csv('./result/recall_0_user0.csv',header=None,skip_blank_lines=False)

for i in range(1,5):
    df_tmp = pd.read_csv('./result/recall_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    df = pd.concat([df,df_tmp],axis=1)

df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]

ndcg = df["sum"]
d = pd.DataFrame(ndcg)
d /= 5
d.to_csv("./result/recall.csv",header=0,index=False)

df = pd.read_csv('./result/f1_0_user0.csv',header=None,skip_blank_lines=False)

for i in range(1,5):
    df_tmp = pd.read_csv('./result/f1_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    df = pd.concat([df,df_tmp],axis=1)

df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]

ndcg = df["sum"]
d = pd.DataFrame(ndcg)
d /= 5
d.to_csv("./result/f1.csv",header=0,index=False)
"""
n = 25
#ndcg
for i in range(5):
    df = pd.read_csv('./result/ndcg_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    for j in range(1,n):
        df_tmp = pd.read_csv('./result/ndcg_{}_user{}.csv'.format(i,j),header=None)
        df = pd.concat([df,df_tmp])
    df = df.reset_index(drop=True)
    df.to_csv("./result/concat_ndcg_{}.csv".format(i))

df = pd.read_csv('./result/concat_ndcg_0.csv',index_col=0,skip_blank_lines=False)
for i in range(1,5):
    df_tmp = pd.read_csv('./result/concat_ndcg_{}.csv'.format(i),index_col=0)
    df = pd.concat([df,df_tmp],axis=1)
df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]
ndcg = df["sum"] 
ndcg /= 5
ndcg.to_csv("./result/ndcg.csv",header=False,index=False)

#precision
for i in range(5):
    df = pd.read_csv('./result/precision_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    for j in range(1,n):
        df_tmp = pd.read_csv('./result/precision_{}_user{}.csv'.format(i,j),header=None)
        df = pd.concat([df,df_tmp])
    df = df.reset_index(drop=True)
    df.to_csv("./result/concat_precision_{}.csv".format(i))

df = pd.read_csv('./result/concat_precision_0.csv',index_col=0,skip_blank_lines=False)
for i in range(1,5):
    df_tmp = pd.read_csv('./result/concat_precision_{}.csv'.format(i),index_col=0)
    df = pd.concat([df,df_tmp],axis=1)
df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]
ndcg = df["sum"] 
ndcg /= 5
ndcg.to_csv("./result/precision.csv",header=False,index=False)

#recall
for i in range(5):
    df = pd.read_csv('./result/recall_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    for j in range(1,n):
        df_tmp = pd.read_csv('./result/recall_{}_user{}.csv'.format(i,j),header=None)
        df = pd.concat([df,df_tmp])
    df = df.reset_index(drop=True)
    df.to_csv("./result/concat_recall_{}.csv".format(i))

df = pd.read_csv('./result/concat_recall_0.csv',index_col=0,skip_blank_lines=False)
for i in range(1,5):
    df_tmp = pd.read_csv('./result/concat_recall_{}.csv'.format(i),index_col=0)
    df = pd.concat([df,df_tmp],axis=1)
df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]
ndcg = df["sum"] 
ndcg /= 5
ndcg.to_csv("./result/recall.csv",header=False,index=False)

#f1
for i in range(5):
    df = pd.read_csv('./result/f1_{}_user0.csv'.format(i),header=None,skip_blank_lines=False)
    for j in range(1,n):
        df_tmp = pd.read_csv('./result/f1_{}_user{}.csv'.format(i,j),header=None)
        df = pd.concat([df,df_tmp])
    df = df.reset_index(drop=True)
    df.to_csv("./result/concat_f1_{}.csv".format(i))

df = pd.read_csv('./result/concat_f1_0.csv',index_col=0,skip_blank_lines=False)
for i in range(1,5):
    df_tmp = pd.read_csv('./result/concat_f1_{}.csv'.format(i),index_col=0)
    df = pd.concat([df,df_tmp],axis=1)
df["sum"] = df.iloc[:,0] + df.iloc[:,1] + df.iloc[:,2] + df.iloc[:,3] + df.iloc[:,4]
ndcg = df["sum"] 
ndcg /= 5
ndcg.to_csv("./result/f1.csv",header=False,index=False)