//
//  hash.cpp
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include "hash.hpp"

using namespace std;

unsigned int MASK[4]={0x00000000,0x000000FF,0x0000FFFF,0x00FFFFFF};

hashing::hashing(int in_size) 
{
  table_size=in_size;
  hash_table=new char*[table_size];
  id_table=new int[table_size];
  hash_clear();
}

void hashing::hash_clear() {
  for(int i=0;i<table_size;i++) hash_table[i]=NULL;
  for(int i=0;i<table_size;i++) id_table[i]=-1;
  stored_num=0;
}
  
unsigned int hashing::hash_val(char *str) {
  unsigned int val=0;
  int pos=0;
  int str_len=strlen(str);
  int block_num=str_len/4;
  int rest_num=str_len%4;
  for(;pos<block_num;pos+=4) {
    val+=*(unsigned int*)(&str[pos]);
  }
  val+=(*(unsigned int*)(&str[pos]))&MASK[rest_num];
  return val%table_size;
}

int hashing::hash_insert(char *str) {
  unsigned int start_pos=hash_val(str);
  unsigned int end_pos=(start_pos-1+table_size)%table_size;
  unsigned int pos=start_pos;
  for(;pos!=end_pos;pos=(pos+1)%table_size) {
    if(hash_table[pos]==NULL) {
      hash_table[pos]=strdup(str);
      id_table[pos]=stored_num;
      stored_num++;
      break;
    }
    else {
      if(strcmp(hash_table[pos],str)==0) break;
    }
  }
  return id_table[pos];
}

int hashing::hash_find(char *str) {
  unsigned int start_pos=hash_val(str);
  unsigned int end_pos=(start_pos-1+table_size)%table_size;
  unsigned int pos=start_pos;
  for(;pos!=end_pos;pos=(pos+1)%table_size) {
    if(hash_table[pos]==NULL) {
      //      cerr<<"not found:"<<str<<endl;
      pos=0xFFFFFFFF;
      break;
    }
    else {
      if(strcmp(hash_table[pos],str)==0) break;
    }
  }
  return (pos!=0xFFFFFFFF)? id_table[pos]: -1;
}
