def split():  
    import pandas as pd
    import numpy as np
    from scipy import sparse
    import math
    from sklearn.model_selection import ShuffleSplit
    import csv
   
    #データの取得
    df_ratings = pd.read_csv('food/RAW_interactions.csv',dtype={'rating':float})
    dR = df_ratings.loc[:,['user_id','recipe_id','rating']]

    #評価を20以上したユーザに制限
    dRR = dR.groupby('user_id').count()['recipe_id'] > 20
    dRR = pd.DataFrame(dRR)
    dRR = dRR.rename(columns={'recipe_id': 'flag'})
    dRR = dRR.reset_index()
    dRR = pd.merge(dR,dRR,how='left')
    dRR = dRR[dRR['flag'] == True]
    dRR = dRR.drop('flag', axis=1)
    dR = dRR
    """"
    #評価を20以上されたアイテムに制限
    dRR = dR.groupby('recipe_id').count()['user_id'] > 1
    dRR = pd.DataFrame(dRR)
    dRR = dRR.rename(columns={'user_id': 'flag'})
    dRR = dRR.reset_index()
    dRR = pd.merge(dR,dRR,how='left')
    dRR = dRR[dRR['flag'] == True]
    dRR = dRR.drop('flag', axis=1)
    dR = dRR
    """

    #user_idの振り直し
    tmp = dR.drop_duplicates(subset='user_id').copy()
    lu = len(tmp)
    tmp['user_ID'] = range(lu)
    tmp = pd.merge(dR, tmp,on='user_id',how='left')
    tmp = tmp.drop(['recipe_id_y', 'rating_y'], axis=1)
    tmp = tmp.rename(columns={'recipe_id_x': 'recipe_id', 'rating_x': 'rating'})


    #recipe_idの振り直し
    tmp2 = tmp.drop_duplicates(subset='recipe_id').copy()
    lm = len(tmp2)

    tmp2['recipe_ID'] = range(lm)
    tmp2 = tmp2.loc[:,['recipe_id','recipe_ID']]
    tmp = pd.merge(tmp, tmp2,on='recipe_id',how='left')
    
    dR = tmp.loc[:,['user_ID','recipe_ID','rating']]

    dR = dR.sample(frac=1, random_state=0)
        
    kf = ShuffleSplit(n_splits=5,random_state=24,test_size=0.4)

    co = 0
    for train, test in kf.split(dR):
        print(co)
        train_df = dR.iloc[train]
        train_df = train_df.reset_index(drop=True)
        test_df = dR.iloc[test]
        test_df = test_df.reset_index(drop=True)


        train_df.to_csv('./split/train_no{}.csv'.format(co))
        test_df.to_csv('./split/test_no{}.csv'.format(co))
        
        co += 1

    return

if __name__ == '__main__':
    split()
