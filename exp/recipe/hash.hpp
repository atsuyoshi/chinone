//
//  hash.hpp
//
#ifndef _INC_HASH
#define _INC_HASH

class hashing {
  int table_size;
  char **hash_table;
  int *id_table;
  unsigned int hash_val(char *str);
public:
  int stored_num;
  hashing(int in_size); 
  void hash_clear();
  int hash_insert(char *str);
  int hash_find(char *str);
};
#endif    //INC_HASH
