import numpy as np
cimport numpy as np
from cpython cimport array
import array
cdef extern from "cppRelationPath_.hpp" namespace "cppRelationPath":
    DEF PATHLENLIMIT = 20     # graph.hpp
    DEF PATHSEQNUM = 10       # graph.hpp
    ctypedef struct PathSeq:
       char type_seq[PATHLENLIMIT]
       double prob
       int count

    ctypedef struct RecInfo:
       int id
       double prob
       PathSeq path_seq[PATHSEQNUM]

    cdef cppclass cppRelationPath:
        cppRelationPath(double *rating_data, int rating_len, long *tag_data, int tag_len, int u_num, int m_num, int t_num,double threshold_prob) except +
        RecInfo *rec_list
        int rec_num
        void set_test_data(double *rating_data, int rating_num)
        void create_rec_buffer(int in_rec_num)
        int recommend(int user_id)

cdef class RelationPath(object):
    cdef cppRelationPath* thisptr

    def __cinit__(self,np.ndarray rating_data,np.ndarray tag_data, int u_num, int m_num, int t_num, float threshold_prob):
        cdef double *double_array = <double*>rating_data.data
        cdef int d_len = len(rating_data)
        cdef long *long_array = <long*>tag_data.data
        cdef int l_len = len(tag_data)
        self.thisptr = new cppRelationPath(double_array,d_len,long_array,l_len,u_num,m_num,t_num,threshold_prob)
        
    def __dealloc__(self):
        del self.thisptr

    def set_test_data(self, np.ndarray rating_data):
        cdef double *double_array = <double*>rating_data.data
        cdef int d_len = len(rating_data)
        self.thisptr.set_test_data(double_array,d_len)

    def create_rec_buffer(self,int in_rec_num):
        self.thisptr.create_rec_buffer(in_rec_num)
	
    def recommend(self, int user_id):
        return self.thisptr.recommend(user_id)

    def get_rec_id(self, int rec_no):
        return self.thisptr.rec_list[rec_no].id

    def get_rec_info(self, int rec_no):
        return self.thisptr.rec_list[rec_no].id, self.thisptr.rec_list[rec_no].prob

    def get_path_info(self, int rec_no, int path_no):
        return self.thisptr.rec_list[rec_no].path_seq[path_no].type_seq, self.thisptr.rec_list[rec_no].path_seq[path_no].prob, self.thisptr.rec_list[rec_no].path_seq[path_no].count 

