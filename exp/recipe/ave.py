#recommend for target user
import sys
from numpy import ma
import pandas as pd
import numpy as np
from scipy import sparse

import math
import csv
def kfold(n,sta,fin,num,user_set):
    #データの取得
    df_ratings = pd.read_csv('food/RAW_interactions.csv')
    dR = df_ratings.loc[:,['user_id','recipe_id','rating']]
    
    #評価数20以上に制限
    dRR = dR.groupby('user_id').count()['recipe_id'] > 20
    dRR = pd.DataFrame(dRR)
    dRR = dRR.rename(columns={'recipe_id': 'flag'})
    dRR = dRR.reset_index()
    dRR = pd.merge(dR,dRR,how='left')
    dRR = dRR[dRR['flag'] == True]
    dRR = dRR.drop('flag', axis=1)
    dR = dRR
    #user_idの振り直し
    tmp = dR.drop_duplicates(subset='user_id').copy()
    
    lu = len(tmp)

    tmp['user_ID'] = range(lu)
    tmp3 = tmp.loc[:,['user_id','user_ID']]
    tmp = pd.merge(dR, tmp,on='user_id',how='left')
    tmp = tmp.drop(['recipe_id_y', 'rating_y'], axis=1)
    tmp = tmp.rename(columns={'recipe_id_x': 'recipe_id', 'rating_x': 'rating'})

    #recipe_idの振り直し
    tmp2 = tmp.drop_duplicates(subset='recipe_id').copy()
    lm = len(tmp2)
   
    ndcg_sum = np.zeros(lu)
    
    precision = np.zeros(lu)
    recall = np.zeros(lu)
    f1 = np.zeros(lu)
    
    with open('./result/precision_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/recall_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/f1_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')

    with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')


    train_df = pd.read_csv('./split/train_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})
    test_df = pd.read_csv('./split/test_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})

    train_df['user_ID'] = train_df['user_ID'] + 1
    test_df['user_ID'] = test_df['user_ID'] + 1
    lr = len(train_df)
    lrt = len(test_df)
    train_df['rating'] = train_df['rating'] + 1
    test_df['rating'] = test_df['rating']  + 1
    
    shape = (lu, lm)
    R = sparse.lil_matrix(shape)
    c = np.zeros((1,lm))
    pre = np.zeros((1,lm))
    for i in range(lr):
        R[train_df.at[i,'user_ID'] - 1, train_df.at[i,'recipe_ID']] = train_df.at[i,'rating']
        c[0,train_df.at[i,'recipe_ID']] += 1
    sum = np.sum(R,axis=0)
    for i in range(lm):
        if sum[0,i] != 0:
            pre[0,i] = sum[0,i] / c[0,i]

    shape_z = (lu, lm)
    Z = sparse.lil_matrix(shape_z)
    
    for i in range(lrt):
        Z[test_df.at[i,'user_ID'] - 1, test_df.at[i,'recipe_ID']]  = test_df.at[i,'rating']

    for u in range(sta,fin):
        if u >= lu:
            break
        print('u: %d'%(u))

        z = np.zeros(lm)
        z = Z[u]
        z = z.todense()
        z = np.ravel(z)

        R_u = [i for i in z if i >= 5]
        l_R_u = len(R_u)
        l_L_n = 0

        x2 = np.zeros(lm)
        #for i in range(10000):
        #    x2[i]= z[np.argsort(x)[::-1][i]] 
        x2 = z[np.argsort(pre)[::-1]]

        x3 = x2[x2 > 0]
        ln = len(x3)

        rate1 = np.zeros(n)
        rate2 = np.zeros(n)            
            
        for i in range(n):
            if(i < ln):
                rate1[i] = x3[i]
            else:
                rate1[i] = 0
            if(rate1[i] >= 5):
                l_L_n += 1
            rate2[i] = np.sort(z)[::-1][i]
            #print(rate1[i],rate2[i])
        
            
        dcg = 0
        idcg = 0
            
        for i in range(n):
            dcg += (2**rate1[i] - 1)/ math.log(i+2,2)
            idcg += (2**rate2[i] - 1)/ math.log(i+2,2)

        ndcg = dcg / idcg

        print('ndcg: '+ str(ndcg))
        ndcg_sum[u] = ndcg
        
        if(l_L_n == 0):
            preci = 0
            reca = 0
            f1m = 0
        else:
            preci = l_L_n / n
            reca = l_L_n / l_R_u
            f1m = 2 * preci * reca / (preci + reca)

        precision[u] = preci
        recall[u] = reca 
        f1[u] = f1m
    

    for u in range(sta,fin):
        if u >= lu:
            break
    
        with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f,lineterminator='\n')
            writer.writerow([ndcg_sum[u]]) 
        with open('./result/precision_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([precision[u]])
        with open('./result/recall_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([recall[u]])
        with open('./result/f1_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([f1[u]]) 
    
    return

if __name__ == '__main__':
    n = 10

    args = sys.argv
    i = int(args[2])
    u_tmp = i + 1
    user_sta = 260 *  i
    user_fin = 260 * u_tmp 
    
    kfold(n,user_sta,user_fin,args[1],i)