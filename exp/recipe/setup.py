#from distutils.core import setup
#from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
from setuptools import setup, Extension
import numpy
setup(
    name = "cppRelationPath",
    ext_modules = cythonize(
        Extension("cppRelationPath",
                  sources=["cppRelationPath.pyx", "cppRelationPath_.cpp", "graph.cpp", "heap.cpp", "hash.cpp", "strbuffer.cpp"],
                  extra_compile_args=["-O3"],
                  language="c++",
                 )
    ),
    include_dirs=[numpy.get_include()]
    #    cmdclass = {'build_ext': build_ext},
)
