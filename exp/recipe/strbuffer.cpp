//
//  strbuffer.cpp
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "strbuffer.hpp"

using namespace std;

strbuffer::strbuffer(int in_size) 
{
  size=in_size;
  buffer=new char[size];
  clear();
}

void strbuffer::clear() {
  tail=0;
}

char *strbuffer::store(char *str) {
  char *copied_str=NULL;
  int str_len=strlen(str)+1;
  if(tail+str_len>size) {
    cerr<<"strbuffer over"<<endl;
  }
  else {
    copied_str=&buffer[tail];
    strcpy(copied_str,str);
    tail+=str_len;
  }
  return copied_str;
}
