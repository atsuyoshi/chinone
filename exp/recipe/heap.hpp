//
//  heap.hpp
//
#ifndef _INC_HEAP
#define _INC_HEAP

class heap {
  double *score;
  int score_num;
public:
  int *rank_list;
  int last_pos;
  int rank_num;
  heap(int size, double *in_score, int in_score_num);
  int insert(int score_pos);
  void insert_top(int score_pos);
  void sort();
};
#endif    //INC_HEAP
