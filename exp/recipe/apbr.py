import sys

def rec(n,num,sta,fin,user_set):  
    import pandas as pd
    import numpy as np
    from scipy import sparse
    import math
    from sklearn.model_selection import KFold
    import csv
    from cppRelationPath import RelationPath

    #データの取得
    df_ratings = pd.read_csv('food/RAW_interactions.csv',dtype={'rating':float})
    dR = df_ratings.loc[:,['user_id','recipe_id','rating']]

    #評価数20以上に制限
    dRR = dR.groupby('user_id').count()['recipe_id'] > 20
    dRR = pd.DataFrame(dRR)
    dRR = dRR.rename(columns={'recipe_id': 'flag'})
    dRR = dRR.reset_index()
    dRR = pd.merge(dR,dRR,how='left')
    dRR = dRR[dRR['flag'] == True]
    dRR = dRR.drop('flag', axis=1)
    dR = dRR

    """"
    #評価を20以上されたアイテムに制限
    dRR = dR.groupby('recipe_id').count()['user_id'] > 1
    dRR = pd.DataFrame(dRR)
    dRR = dRR.rename(columns={'user_id': 'flag'})
    dRR = dRR.reset_index()
    dRR = pd.merge(dR,dRR,how='left')
    dRR = dRR[dRR['flag'] == True]
    dRR = dRR.drop('flag', axis=1)
    dR = dRR
    """

    #user_idの振り直し
    tmp = dR.drop_duplicates(subset='user_id').copy()
    lu = len(tmp)
    tmp['user_ID'] = range(lu)
    tmp3 = tmp.loc[:,['user_id','user_ID']]
    tmp = pd.merge(dR, tmp,on='user_id',how='left')
    tmp = tmp.drop(['recipe_id_y', 'rating_y'], axis=1)
    tmp = tmp.rename(columns={'recipe_id_x': 'recipe_id', 'rating_x': 'rating'})

    #recipe_idの振り直し
    tmp2 = tmp.drop_duplicates(subset='recipe_id').copy()
    lm = len(tmp2)
    tmp2['recipe_ID'] = range(lm)
    tmp2 = tmp2.loc[:,['recipe_id','recipe_ID']]
    tmp = pd.merge(tmp, tmp2,on='recipe_id',how='left')
    
    dR = tmp.loc[:,['user_ID','recipe_ID','rating']]

    #タグ
    df_tags = pd.read_csv('food/RAW_recipes.csv')
    dT = df_tags.loc[:, ['id','contributor_id','tags']]
    #タグだけついていて、評価されていないレシピがあるからNANになってしまう
    dT = pd.merge(dT, tmp2,how='left',left_on='id',right_on='recipe_id').drop(columns=['id','recipe_id'])
    #tagging is 231637
    dT = dT.dropna()
    #tagging is 197857   
    #タグのみ作り評価をしていないユーザがいるからNANになる
    dT = pd.merge(dT, tmp3,how='left',left_on='contributor_id',right_on='user_id').drop(columns=['contributor_id','user_id'])
    #タグのみ作り評価をしていないデータを削除,row_num is213598
    dT = dT.dropna()
    #tagging is 153684
    dT['user_ID'] = dT['user_ID'].astype('int')
    dT['recipe_ID'] = dT['recipe_ID'].astype('int')

    
    #タグの[,]を削除
    dT['tags'] = dT['tags'].str.replace(']','')
    dT['tags'] = dT['tags'].str.replace('[','')
    #タグをカウント(lta=552)
    dT['tag_list'] = dT['tags'].str.split(', ')
    all_tags = set()
    for this_recipe_tags in dT['tag_list']:
        all_tags = all_tags.union(this_recipe_tags)
    all_tags = list(all_tags)
    #タグのdf
    tagg = pd.DataFrame(all_tags,columns = ['tag'])
    tagg['tagId'] = range(len(tagg))
    lta = len(tagg)

    #tag_listを複数行に展開
    lst_col = "tag_list"
    df_new = pd.DataFrame({col:np.repeat(dT[col].values, dT[lst_col].str.len())for col in dT.columns.difference([lst_col])}).assign(**{lst_col:np.concatenate(dT[lst_col].values)})[dT.columns.tolist()]
    new_dT = df_new.loc[:,['user_ID','recipe_ID','tag_list']]
    dff = pd.merge(new_dT,tagg,left_on='tag_list',right_on='tag')
    lt = len(dff)

    dT = dff.loc[:,['user_ID','recipe_ID','tagId']]
    #ndcg_sum = np.zeros(lu)
    
    precision = np.zeros(lu)
    recall = np.zeros(lu)
    f1 = np.zeros(lu)
    
    with open('./result/precision_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/recall_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/f1_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n') 
    with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')

    train_df = pd.read_csv('./split/train_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})
    test_df = pd.read_csv('./split/test_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})

    train_df['user_ID'] = train_df['user_ID'] + 1
    test_df['user_ID'] = test_df['user_ID']  + 1
    dT['user_ID'] = dT['user_ID']  + 1
    train_df['rating'] = train_df['rating'] + 1
    test_df['rating'] = test_df['rating']  + 1

    lrt = len(test_df)

    rp=RelationPath(train_df.values,dT.values,lu,lm,lta,0.0000001)
    
    rp.set_test_data(test_df.values)
    rp.create_rec_buffer(n)

    shape_z = (lu, lm)
    Z = sparse.lil_matrix(shape_z)

    for i in range(lrt):
        Z[test_df.at[i,'user_ID'] -1, test_df.at[i,'recipe_ID']]  = test_df.at[i,'rating']

    for u in range(3152,3153):
        if u >= lu:
            break
        print('u: %d'%(u))
        rec_num=rp.recommend(u)
        
        for i in range(rec_num):
            user_id,prob=rp.get_rec_info(i)
            print(user_id,":",prob)
            for j in range(10):
                type_seq,prob,count=rp.get_path_info(i,j)
                if(prob<0):
                    break
                print("[",type_seq,",",prob,",",count,"]",end='')
            print()
                    
        z = np.zeros(lm)
        z = Z[u]
        z = z.todense()
        z = np.ravel(z)
        
        R_u = [i for i in z if i >= 5]
        l_R_u = len(R_u)
        l_L_n = 0
        
        rate1 = np.zeros(n)
        rate2 = np.zeros(n)            
        
        for i in range(n):
            if(i < rec_num):
                rate1[i] = z[rp.get_rec_id(i)]
            else:
                rate1[i] = 0
            if(rate1[i] >= 5):
                l_L_n += 1
            rate2[i] = np.sort(z)[::-1][i]
            print(rate1[i],rate2[i])


        dcg = 0
        idcg = 0
            
        for i in range(n):
            dcg += (2**rate1[i] - 1)/ math.log(i+2,2)
            idcg += (2**rate2[i] - 1)/ math.log(i+2,2)
        """
        dcg = rate1[0]
        idcg = rate2[0]
            
        for i in range(1,n):
            dcg += rate1[i] / math.log(i+1,2)
            idcg += rate2[i] / math.log(i+1,2)
        """
        ndcg = dcg / idcg

        print('ndcg: '+ str(ndcg))
        ndcg_sum[u] = ndcg
        
        if(l_L_n == 0):
            preci = 0
            reca = 0
            f1m = 0
        else:
            preci = l_L_n / n
            reca = l_L_n / l_R_u
            f1m = 2 * preci * reca / (preci + reca)

        precision[u] = preci
        recall[u] = reca 
        f1[u] = f1m

    for u in range(sta,fin):
        if u >= lu:
            break
    
        with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f,lineterminator='\n')
            writer.writerow([ndcg_sum[u]]) 
        
     
        with open('./result/precision_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([precision[u]])
        with open('./result/recall_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([recall[u]])
        with open('./result/f1_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([f1[u]])
        

    return


if __name__ == '__main__':
    args = sys.argv
    i = int(args[2])
    u_tmp = i + 1
    
    user_sta = 260 *  i
    user_fin = 260 * u_tmp 

    rec(10,args[1],user_sta,user_fin,i)
