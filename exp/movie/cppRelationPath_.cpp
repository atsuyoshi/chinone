//
//  cppRelationPath_.cpp
//
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <iostream>
#include <functional>
#include "cppRelationPath_.hpp"



using namespace std;


namespace cppRelationPath{

  cppRelationPath::cppRelationPath(double *rating_data, int rating_num, long *tag_data, int tag_num, int u_num, int m_num, int t_num, double threshold_prob) {
  cout<<"user_num="<<u_num<<endl;
  cout<<"movie_num="<<m_num<<endl;
  cout<<"tag_name_num="<<t_num<<endl;
  cout<<"threshold_prob="<<threshold_prob<<endl;

  user_num=u_num;
  movie_num=m_num;
  node_num=user_num+m_num+t_num;

  cout << "node_num="<< node_num<<endl;
  
  int *edge_num=new int[node_num];
  for(int i=0;i<node_num;i++) edge_num[i]=0; 
  user_edge_num=edge_num;
  movie_edge_num=&edge_num[user_num];
  tag_edge_num=&movie_edge_num[movie_num];

  // count rating
  for(int i=0;i<rating_num;i++) {
    user_edge_num[int(rating_data[i])-1]++;
    movie_edge_num[int(rating_data[rating_num+i])]++;
  }
  // count tag
  for(int i=0;i<tag_num;i++) {
    user_edge_num[int(tag_data[i])-1]+=2;
    movie_edge_num[int(tag_data[tag_num+i])]+=2;
    tag_edge_num[int(tag_data[2*tag_num+i])]+=2;    
  }

  G=new graph(node_num,edge_num,u_num,m_num,threshold_prob);

  set_rating_weight(rating_data,rating_num);
  set_tag_weight(tag_data,tag_num);

  rating_weight_normalization();
  merge_tag_weight();
  tag_weight_normalization();
  weight_normalization();
}

cppRelationPath::~cppRelationPath() {
}

void
cppRelationPath::set_rating_weight(double *rating_data,int rating_num) {
  for(int i=0;i<rating_num;i++) {
    int user_id=int(rating_data[i])-1;
    int movie_id=int(rating_data[rating_num+i])+user_num;
    double rating=rating_data[2*rating_num+i];
    G->adjacent_list[user_id][G->edge_pos[0][user_id]].to=movie_id;
    G->adjacent_list[user_id][G->edge_pos[0][user_id]].weight=rating;
    G->edge_pos[0][user_id]++;
    G->adjacent_list[movie_id][G->edge_pos[0][movie_id]].to=user_id;
    G->adjacent_list[movie_id][G->edge_pos[0][movie_id]].weight=rating;
    G->edge_pos[0][movie_id]++;
  }
  for(int i=0;i<user_num+movie_num;i++) G->edge_pos[1][i]=G->edge_pos[0][i];
}

void
cppRelationPath::set_tag_weight(long *tag_data, int tag_num) {
  for(int i=0;i<tag_num;i++) {
    int user_id=int(tag_data[i])-1;
    int movie_id=int(tag_data[tag_num+i])+user_num;
    int tag_id=int(tag_data[2*tag_num+i])+user_num+movie_num;
    G->adjacent_list[user_id][G->edge_pos[1][user_id]].to=movie_id;
    G->adjacent_list[user_id][G->edge_pos[1][user_id]].weight=1;
    G->edge_pos[1][user_id]++;
    G->adjacent_list[user_id][G->edge_pos[1][user_id]].to=tag_id;
     G->adjacent_list[user_id][G->edge_pos[1][user_id]].weight=1;
    G->edge_pos[1][user_id]++;
    G->adjacent_list[movie_id][G->edge_pos[1][movie_id]].to=user_id;
     G->adjacent_list[movie_id][G->edge_pos[1][movie_id]].weight=1;
    G->edge_pos[1][movie_id]++;
    G->adjacent_list[movie_id][G->edge_pos[1][movie_id]].to=tag_id;
     G->adjacent_list[movie_id][G->edge_pos[1][movie_id]].weight=1;
    G->edge_pos[1][movie_id]++;
    G->adjacent_list[tag_id][G->edge_pos[1][tag_id]].to=user_id;
     G->adjacent_list[tag_id][G->edge_pos[1][tag_id]].weight=1;
    G->edge_pos[1][tag_id]++;
    G->adjacent_list[tag_id][G->edge_pos[1][tag_id]].to=movie_id;
     G->adjacent_list[tag_id][G->edge_pos[1][tag_id]].weight=1;
    G->edge_pos[1][tag_id]++;
  }
}

void
cppRelationPath::rating_weight_normalization() {
  int um_num=user_num+movie_num;
  double *weight_norm=new double[um_num];
  for(int i=0;i<um_num;i++) weight_norm[i]=0;
  for(int i=0;i<um_num;i++) {
    for(int j=0;j<G->edge_pos[0][i];j++) {
	weight_norm[i]+=G->adjacent_list[i][j].weight;
	/*	weight_norm[G->adjacent_list[i][j].to]+=G->adjacent_list[i][j].weight; */
    }
  }
  for(int i=0;i<um_num;i++) weight_norm[i]=sqrt(weight_norm[i]);
  for(int i=0;i<um_num;i++) {
    float weight_sum=0;
    for(int j=0;j<G->edge_pos[0][i];j++) {
        G->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[G->adjacent_list[i][j].to];
        weight_sum+=G->adjacent_list[i][j].weight;
     }    
    for(int j=0;j<G->edge_pos[0][i];j++) G->adjacent_list[i][j].weight/=weight_sum/G->edge_pos[0][i];
  }
  delete[] weight_norm;
}

int compare_to(const void *a, const void *b)
{
  return ((Edge*)a)->to - ((Edge*)b)->to;
}

void
cppRelationPath::merge_tag_weight() {
  for(int i=0;i<node_num;i++) {
    int size=G->edge_pos[1][i]-G->edge_pos[0][i];
    if(size>1) {
      qsort(&(G->adjacent_list[i][G->edge_pos[0][i]]),size,sizeof(Edge),compare_to);
      int cur_pos=G->edge_pos[0][i];
      for(int j=cur_pos+1;j<G->edge_pos[1][i];j++) {
	if(G->adjacent_list[i][j].to==G->adjacent_list[i][cur_pos].to) G->adjacent_list[i][cur_pos].weight+=G->adjacent_list[i][j].weight;
	else {
	  cur_pos++;
	  G->adjacent_list[i][cur_pos]=G->adjacent_list[i][j];
	}
      }
      G->edge_pos[1][i]=cur_pos+1;
    }
  }
}

void
cppRelationPath::tag_weight_normalization() {
  double *weight_norm=new double[node_num];
  for(int i=0;i<node_num;i++) weight_norm[i]=0;
  for(int i=0;i<node_num;i++) {
    for(int j=G->edge_pos[0][i];j<G->edge_pos[1][i];j++) {
	weight_norm[i]+=G->adjacent_list[i][j].weight;
	/*	weight_norm[G->adjacent_list[i][j].to]+=G->adjacent_list[i][j].weight; */
    }
  }
  for(int i=0;i<node_num;i++) weight_norm[i]=sqrt(weight_norm[i]);
  int um_num=user_num+movie_num;
  for(int i=0;i<um_num;i++) {
    float weight_sum=0;
    int j=G->edge_pos[0][i];
    for(;j<G->edge_pos[1][i];j++) {
        if(G->adjacent_list[i][j].to>=um_num) break;
        G->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[G->adjacent_list[i][j].to];
        weight_sum+=G->adjacent_list[i][j].weight;
    }
    int k=G->edge_pos[0][i];
    int size=j-k;
    for(;k<j;k++) G->adjacent_list[i][k].weight/=weight_sum/size;
    weight_sum=0;
    for(;j<G->edge_pos[1][i];j++) {
        G->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[G->adjacent_list[i][j].to];
        weight_sum+=G->adjacent_list[i][j].weight;
    }
    size=G->edge_pos[1][i]-k;
    for(;k<G->edge_pos[1][i];k++) G->adjacent_list[i][k].weight/=weight_sum/size;
  }
  for(int i=um_num;i<node_num;i++) {
    float weight_sum=0;
    int j=G->edge_pos[0][i];
    for(;j<G->edge_pos[1][i];j++) {
        if(G->adjacent_list[i][j].to>=user_num) break;
        G->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[G->adjacent_list[i][j].to];
        weight_sum+=G->adjacent_list[i][j].weight;
    }
    int k=G->edge_pos[0][i];
    int size=j-k;
    for(;k<j;k++) G->adjacent_list[i][k].weight/=weight_sum/size;
    weight_sum=0;
    for(;j<G->edge_pos[1][i];j++) {
        G->adjacent_list[i][j].weight/=weight_norm[i]*weight_norm[G->adjacent_list[i][j].to];
        weight_sum+=G->adjacent_list[i][j].weight;
    }
    size=G->edge_pos[1][i]-k;
    for(;k<G->edge_pos[1][i];k++) G->adjacent_list[i][k].weight/=weight_sum/size;
  }
  delete[] weight_norm;
}
  
void
cppRelationPath::weight_normalization() {
  for(int i=0;i<node_num;i++) {
    float degree=0;
    for(int j=0;j<G->edge_pos[1][i];j++) {
      degree+=G->adjacent_list[i][j].weight;
    }
    for(int j=0;j<G->edge_pos[1][i];j++) {
      G->adjacent_list[i][j].weight/=degree;
    }
    /*****************************************
    int j=0;
    for(;j<G->edge_pos[0][i];j++) {
      degree+=G->adjacent_list[i][j].weight;
    }
    for(;j<G->edge_pos[1][i];j++) {
      degree+=G->adjacent_list[i][j].weight/2;
    }
    j=0;
    for(;j<G->edge_pos[0][i];j++) {
      G->adjacent_list[i][j].weight/=2*degree;
    }
    for(;j<G->edge_pos[1][i];j++) {
      G->adjacent_list[i][j].weight/=3*degree;
    }
    *****************************************/
  }
}

void
cppRelationPath::set_test_data(double *rating_data, int rating_num) {
  rated_movie_area=new int[rating_num];
  rated_movie=new int*[user_num];
  rated_num=new int[user_num];
  for(int i=0;i<user_num;i++) rated_num[i]=0;
  for(int i=0;i<rating_num;i++) rated_num[int(rating_data[i])-1]++;
  int pos=0;
  for(int i=0;i<user_num;i++) {
    rated_movie[i]=&rated_movie_area[pos];
    pos+=rated_num[i];
    rated_num[i]=0;
  }
  for(int i=0;i<rating_num;i++) {
    int user_id=int(rating_data[i])-1;
    rated_movie[user_id][rated_num[user_id]++]=int(rating_data[rating_num+i]);
  }
}

void
cppRelationPath::create_rec_buffer(int in_rec_num) {
  rec_num=in_rec_num;
  rec_list=new RecInfo[rec_num];
}
  
int
cppRelationPath::recommend(int user_id) {
  return G->recommend(user_id,rec_num,rec_list,rated_movie[user_id],rated_num[user_id]);
}
  
}
