def split():  
    import pandas as pd
    import numpy as np
    from scipy import sparse
    import math
    from sklearn.model_selection import ShuffleSplit
    import csv
   

    #データの取得
    df_tags = pd.read_csv('ml-20m/tags.csv')
    df_movies = pd.read_csv('ml-20m/movies.csv')
    df_movies = df_movies.drop('genres',axis=1)
    df_ratings = pd.read_csv('ml-20m/ratings.csv')

    df_tags_combined = df_tags.groupby('movieId').apply(lambda x: list(x['tag'])).reset_index().rename(columns={0:'tags'})
    dfm = pd.merge(df_movies, df_tags_combined, on = 'movieId', how = 'left')
    dfm['tags'] = dfm['tags'].apply(lambda x: x if isinstance(x,list) else [])
    all_tags = set()
    for this_movie_tags in dfm['tags']:
        all_tags = all_tags.union(this_movie_tags)
    all_tags = list(all_tags)
    tagg = pd.DataFrame(all_tags,columns = ['tag'])
    tagg['tagId'] = range(len(tagg))
    dff = pd.merge(df_tags,tagg)

    lu = 138493
    lm = len(df_movies)


    """
    #間違っていた
    df_movies['movieId'] = range(lm)
    dfr = pd.merge(df_movies, df_ratings, on = 'movieId')
    """
    df_movies['movieid'] = range(lm)
    dfr = pd.merge(df_movies, df_ratings, on = 'movieId')

    dR = dfr.loc[:,['userId','movieid','rating']]
    dR = dR.sample(frac=1, random_state=0)

   
    kf = ShuffleSplit(n_splits=5,random_state=24,test_size=0.4)

    co = 0
    for train, test in kf.split(dR):
        print(co)
        train_df = dR.iloc[train]
        train_df = train_df.reset_index(drop=True)
        test_df = dR.iloc[test]
        test_df = test_df.reset_index(drop=True)


        train_df.to_csv('./split/train_no{}.csv'.format(co))
        test_df.to_csv('./split/test_no{}.csv'.format(co))
        
        co += 1
        
    return

if __name__ == '__main__':
    split()
