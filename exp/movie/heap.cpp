//
//  heap.cpp
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include "heap.hpp"

using namespace std;

heap::heap(int size, double *in_score, int in_score_num) {
  rank_num=size;
  rank_list=new int[rank_num];
  last_pos=0;
  score=in_score;
  score_num=in_score_num;
}

int 
heap::insert(int score_pos) {
  int insert_pos=-1;
  if(last_pos<rank_num) {
    insert_pos=last_pos;
    while(insert_pos>0) {
      int parent_pos=(insert_pos-1)/2;
      int spos=rank_list[parent_pos];
      if(score[spos]<=score[score_pos]) break;
      rank_list[insert_pos]=spos;
      insert_pos=parent_pos;
    }
    rank_list[insert_pos]=score_pos;
    last_pos++;
  }
  return insert_pos;
}

void
heap::insert_top(int score_pos) {
  int insert_pos=0;
  int child_pos=1;
  while(child_pos<last_pos) {
    if(child_pos+1<last_pos) {
      if(score[rank_list[child_pos]]>score[rank_list[child_pos+1]]) child_pos++;
    }
    int spos=rank_list[child_pos];
    if(score[spos]>=score[score_pos]) break;
    rank_list[insert_pos]=spos;
    insert_pos=child_pos;
    child_pos=2*insert_pos+1;
  }
  rank_list[insert_pos]=score_pos;
  return;
}

void
heap::sort() {
  int save_last_pos=last_pos;
  while(last_pos>1) {
    last_pos--;
    int temp=rank_list[last_pos];
    rank_list[last_pos]=rank_list[0];
    insert_top(temp);    
  }
  last_pos=save_last_pos;
}
