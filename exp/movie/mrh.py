#recommend for target user
import sys

def kfold(pra,ite,n,sta,fin,num,user_set):  
    import pandas as pd
    import numpy as np
    from scipy import sparse
    import math
    from sklearn.model_selection import KFold
    import csv

    #データの取得
    df_tags = pd.read_csv('ml-20m/tags.csv')
    df_movies = pd.read_csv('ml-20m/movies.csv')
    df_movies = df_movies.drop('genres',axis=1)
    #df_ratings = pd.read_csv('ml-20m/ratings.csv')

    df_tags_combined = df_tags.groupby('movieId').apply(lambda x: list(x['tag'])).reset_index().rename(columns={0:'tags'})
    dfm = pd.merge(df_movies, df_tags_combined, on = 'movieId', how = 'left')
    dfm['tags'] = dfm['tags'].apply(lambda x: x if isinstance(x,list) else [])
    all_tags = set()
    for this_movie_tags in dfm['tags']:
        all_tags = all_tags.union(this_movie_tags)
    all_tags = list(all_tags)
    tagg = pd.DataFrame(all_tags,columns = ['tag'])
    tagg['tagId'] = range(len(tagg))
    dff = pd.merge(df_tags,tagg)

    lu = 138493
    lm = len(df_movies)
    lt = len(df_tags)
    lta = len(tagg)
    
    #要注意
    df_movies['movieid'] = range(lm)
    
    ndcg_sum = np.zeros(lu)
    precision = np.zeros(lu)
    recall = np.zeros(lu)
    f1 = np.zeros(lu)
    
    with open('./result/precision_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/recall_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/f1_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')

    dft = pd.merge(dff,df_movies)
    dT = dft.loc[:,['userId','movieid','tagId']]

    
    with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
   
    train_df = pd.read_csv('./split/train_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})
    test_df = pd.read_csv('./split/test_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})


    lr = len(train_df)
    lrt = len(test_df)

    shape = (lu, lm)
    R = sparse.lil_matrix(shape)

    shape_h = (lu + lm + lta, lr + lt)
    H = sparse.lil_matrix(shape_h)
    c = np.zeros((lu,1))
        
    shape_w = (lr + lt, lr + lt)
    W = sparse.lil_matrix(shape_w)

    shape_dv = (lu + lm + lta, lu + lm + lta)
    D_v = sparse.lil_matrix(shape_dv)
    D_v_inv = sparse.lil_matrix(shape_dv)
    v_c = np.zeros(lu + lm + lta)

    shape_de = (lr + lt, lr + lt)
    D_e = sparse.lil_matrix(shape_de) 
    D_e_inv = sparse.lil_matrix(shape_de)
   
    for i in range(lr):
        R[train_df.at[i,'userId'] - 1, train_df.at[i,'movieid']] = train_df.at[i,'rating']
        H[train_df.at[i,'userId'] - 1,i] = 1
        H[lu + train_df.at[i,'movieid'],i] = 1
        c[train_df.at[i,'userId'] - 1] += 1
    sum1 = np.sum(R, axis=1) 
    sum2 = np.sum(R, axis=0)  
    ave = sum1 / c 

    for i in range(lr):
        W[i,i] = train_df.at[i,'rating'] / (np.sqrt(sum1[train_df.at[i,'userId'] - 1]) * np.sqrt(sum2[0,train_df.at[i,'movieid']]))
        W[i,i] /= ave[train_df.at[i,'userId'] - 1]
        v_c[train_df.at[i,'userId'] - 1] += W[i,i]
        v_c[lu + train_df.at[i,'movieid']] += W[i,i] 

  
    for i in range(lt):
        H[lu + lm + dT.at[i,'tagId'], lr + i] = 1
        H[dT.at[i,'userId'] - 1,lr + i] = 1
        H[lu + dT.at[i,'movieid'] ,lr + i] = 1
        W[lr + i, lr + i] = 1 
        v_c[lu + lm + dT.at[i,'tagId']] += W[lr + i, lr + i]
        v_c[dT.at[i,'userId'] - 1] += W[lr + i, lr + i]
        v_c[lu + dT.at[i,'movieid']] += W[lr + i, lr + i]
    
    for i in range(lu + lm + lta):
        D_v[i,i] = v_c[i]
    
    e_c = np.sum(H, axis=0)
    for i in range(lr + lt):
        D_e[i,i] = e_c[0,i]

    for i in range(lu + lm + lta):
        if D_v[i,i] != 0:
            D_v_inv[i,i] = np.reciprocal(D_v[i,i])

    for i in range(lr + lt):
        if D_e[i,i] != 0:
            D_e_inv[i,i] = np.reciprocal(D_e[i,i])
    
    #遷移行列を求める        
    A = D_v_inv * H * W * D_e_inv * H.T
    
    
    shape_z = (lu, lm)
    Z = sparse.lil_matrix(shape_z)
    for i in range(lrt):
        Z[test_df.at[i,'userId'] - 1, test_df.at[i,'movieid']]  = test_df.at[i,'rating']
    
    for u in range(sta,fin):
        if u >= lu:
            break
            
        print('u: %d'%(u))
        t_u = u
        #クエリを立てる
        y = A[t_u].copy()
        y[0,t_u] = 1
        y = y / np.sum(y)
        f = y.copy()
        old_f = y.copy()
            
        #set a pararumeter
        c = pra
        t = ite
        #ランダムウォーク
        for i in range(t):
            f = (c * old_f * A) + ((1-c)*y)
            if(i != t-1):
                old_f = f.copy()

        x = f[0,lu:lu+lm].copy()
        x = x.todense()
        x = np.ravel(x)
            
        #test_dataのtarget_userのratings
        z = np.zeros(lm)
        z = Z[u]
        z = z.todense()
        z = np.ravel(z)
        R_u = [i for i in z if i >= 4]
        l_R_u = len(R_u)
        l_L_n = 0

        x2 = np.zeros(lm)
        #for i in range(6000):
        #    x2[i]= z[np.argsort(x)[::-1][i]]
        #x2[np.argsort(x)[::-1]] = z   
        x2 = z[np.argsort(x)[::-1]]

        x3 = x2[x2 > 0]
        ln = len(x3)
        #check efiiciency

        rate1 = np.zeros(n)
        rate2 = np.zeros(n)
            
            
        for i in range(n):
            if(i < ln):
                rate1[i] = x3[i]
            else:
                rate1[i] = 0
            if(rate1[i] >= 4):
                l_L_n += 1
            rate2[i] = np.sort(z)[::-1][i]
            #print(rate1[i],rate2[i])
            
        dcg = 0
        idcg = 0
            
        for i in range(n):
            dcg += (2**rate1[i] - 1)/ math.log(i+2,2)
            idcg += (2**rate2[i] - 1)/ math.log(i+2,2)

        ndcg = dcg / idcg

        print('ndcg: '+ str(ndcg))
        ndcg_sum[u] = ndcg
        if(l_L_n == 0):
            preci = 0
            reca = 0
            f1m = 0
        else:
            preci = l_L_n / n
            reca = l_L_n / l_R_u
            f1m = 2 * preci * reca / (preci + reca)

        precision[u] = preci
        recall[u] = reca 
        f1[u] = f1m


    for u in range(sta,fin):
        if u >= lu:
            break
        with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f,lineterminator='\n')
            writer.writerow([ndcg_sum[u]])
        with open('./result/precision_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([precision[u]])
        with open('./result/recall_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([recall[u]])
        with open('./result/f1_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([f1[u]])
    
    return

if __name__ == '__main__':
    c = 0.96
    t = 80
    n = 10

    args = sys.argv
    i = int(args[2])
    u_tmp = i + 1
    user_sta = 5600 *  i
    user_fin = 5600 * u_tmp 
    
    kfold(c,t,n,user_sta,user_fin,args[1],i)