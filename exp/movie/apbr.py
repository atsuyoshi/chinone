import sys

def rec(n,num,sta,fin,user_set):  
    import pandas as pd
    import numpy as np
    from scipy import sparse
    import math
    from sklearn.model_selection import KFold
    import csv
    from cppRelationPath import RelationPath

    #データの取得
    
    df_tags = pd.read_csv('ml-20m/tags.csv')
    df_movies = pd.read_csv('ml-20m/movies.csv')
    df_movies = df_movies.drop('genres',axis=1)
    

    df_tags_combined = df_tags.groupby('movieId').apply(lambda x: list(x['tag'])).reset_index().rename(columns={0:'tags'})
    dfm = pd.merge(df_movies, df_tags_combined, on = 'movieId', how = 'left')
    dfm['tags'] = dfm['tags'].apply(lambda x: x if isinstance(x,list) else [])
    all_tags = set()
    for this_movie_tags in dfm['tags']:
        all_tags = all_tags.union(this_movie_tags)
    all_tags = list(all_tags)
    tagg = pd.DataFrame(all_tags,columns = ['tag'])
    tagg['tagId'] = range(len(tagg))
    dff = pd.merge(df_tags,tagg)

    lu = 138493
    lm = len(df_movies)
    lt = len(df_tags)
    lta = len(tagg)
    """
    #ミス
    df_movies['movieId'] = range(lm)
    """
    df_movies['movieid'] = range(lm)
    ndcg_sum = np.zeros(lu)
    precision = np.zeros(lu)
    recall = np.zeros(lu)
    f1 = np.zeros(lu)
    dft = pd.merge(dff,df_movies)
    dT = dft.loc[:,['userId','movieid','tagId']]


    with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/precision_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/recall_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')
    with open('./result/f1_{}_user{}.csv'.format(num,user_set),'w') as f:
        writer = csv.writer(f, lineterminator='\n')

    train_df = pd.read_csv('./split/train_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})
    test_df = pd.read_csv('./split/test_no{}.csv'.format(num),index_col=0,dtype={'0':int,'1':int,'2':float})

    lrt = len(test_df)

    rp=RelationPath(train_df.values,dT.values,lu,lm,lta,0.0000001)

    rp.set_test_data(test_df.values)
    rp.create_rec_buffer(n)

    shape_z = (lu, lm)
    Z = sparse.lil_matrix(shape_z)
    
    for i in range(lrt):
        Z[test_df.at[i,'userId'] - 1, test_df.at[i,'movieid']]  = test_df.at[i,'rating']

    for u in range(sta,fin):
        if u >= lu:
            break
        print('u: %d'%(u))
        rec_num=rp.recommend(u)

        for i in range(rec_num):
            user_id,prob=rp.get_rec_info(i)
            print(user_id,":",prob)
            for j in range(10):
                type_seq,prob,count=rp.get_path_info(i,j)
                if(prob<0):
                    break
                print("[",type_seq,",",prob,",",count,"]",end='')
            print()
                    #test_dataのtarget_userのratings
        z = np.zeros(lm)
        z = Z[u]
        z = z.todense()
        z = np.ravel(z)

        R_u = [i for i in z if i >= 4]
        l_R_u = len(R_u)
        l_L_n = 0

        rate1 = np.zeros(n)
        rate2 = np.zeros(n)            
            
        for i in range(n):
            if(i < rec_num):
                rate1[i] = z[rp.get_rec_id(i)]
            else:
                rate1[i] = 0
            if(rate1[i] >= 4):
                l_L_n += 1
            rate2[i] = np.sort(z)[::-1][i]
            #print(rate1[i],rate2[i])
            
        dcg = 0
        idcg = 0
            
        for i in range(n):
            dcg += (2**rate1[i] - 1)/ math.log(i+2,2)
            idcg += (2**rate2[i] - 1)/ math.log(i+2,2)

        ndcg = dcg / idcg

        print('ndcg: '+ str(ndcg))
        ndcg_sum[u] = ndcg
        if(l_L_n == 0):
            preci = 0
            reca = 0
            f1m = 0
        else:
            preci = l_L_n / n
            reca = l_L_n / l_R_u
            f1m = 2 * preci * reca / (preci + reca)

        precision[u] = preci
        recall[u] = reca 
        f1[u] = f1m
    

    for u in range(sta,fin):
        if u >= lu:
            break
        with open('./result/ndcg_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f,lineterminator='\n')
            writer.writerow([ndcg_sum[u]])     
        with open('./result/precision_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([precision[u]])
        with open('./result/recall_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([recall[u]])
        with open('./result/f1_{}_user{}.csv'.format(num,user_set),'a') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([f1[u]])
    return


if __name__ == '__main__':
    args = sys.argv
    i = int(args[2])
    u_tmp = i + 1
    
    user_sta = 5600 *  i
    user_fin = 5600 * u_tmp 

    rec(10,args[1],user_sta,user_fin,i)
