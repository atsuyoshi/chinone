//
//  graph.cpp
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include "graph.hpp"
#include "heap.hpp"

using namespace std;

char path_type[8]={'R','T','U','r','t','M','u','m'}; //R: rating, U:user-movie (tag), T:usr-tag, M:movie-tag

unsigned int mask=0xF0000000;

char letter[16]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

graph::graph(int in_node_num, int *edge_num, int user_num, int item_num, double in_threshold_prob) 
{
  node_num=in_node_num;
  threshold_prob=in_threshold_prob;
  item_head=user_num;
  item_tail=user_num+item_num;
  area_size=0;
  for(int i=0;i<node_num;i++) area_size+=edge_num[i];
  adjacent_list_area=new Edge[area_size];
  adjacent_list=new Edge*[node_num];
  edge_pos[0]=new int[node_num];
  edge_pos[1]=new int[node_num];
  visited=new unsigned char[node_num];
  int cumulative_pos=0;
  for(int i=0;i<node_num;i++) {
    edge_pos[0][i]=0;
    edge_pos[1][i]=0;
    adjacent_list[i]=&adjacent_list_area[cumulative_pos];
    cumulative_pos+=edge_num[i];
    visited[i]=0;
  }
  prob_sum=new double[node_num];
  for(int i=0;i<item_head;i++) prob_sum[i]=-1;
  for(int i=item_tail;i<node_num;i++) prob_sum[i]=-1; 
  strbuf =new strbuffer(DIFPATHNUM*KEY_LEN);
  path_table=new hashing(DIFPATHNUM*2);
  path_list=new PathInfo[DIFPATHNUM];
  path_prob_sum=new double[DIFPATHNUM];
  ranking=new int[item_tail-item_head];
}

char
graph::tag_type(int id_type, int to_id) {
  int type_no=id_type;
  if(id_type<6) {
    if(to_id<item_tail) {
      type_no++;
    }
    else {
      type_no+=2;
    }
  }
  else {
    type_no+=(to_id<item_head)? 0:1;
  }
  return path_type[type_no];
}

char
*graph::path_key(int id,int depth) {
  for(int i=0;i<8;i++) key_buf[i]=letter[(id&(mask>>(i*4)))>>((7-i)*4)];    
  for(int i=0;i<depth;i++) key_buf[i+8]=path[i];
  key_buf[8+depth]=0;
  return key_buf;
}

int
graph::find_path(int id, double cur_prob, int depth) {
  int status=0;
  if(depth>=PATHLENLIMIT) return 1;
  visited[id]=1;
  if(prob_sum[id]>=0) {
    int path_no;
    if((path_no=path_table->hash_find(path_key(id,depth)))<0) {
      char *key_str=strbuf->store(key_buf);
      if((path_no=path_table->hash_insert(key_str))>=DIFPATHNUM) {
	cerr<<"#path is more than "<<DIFPATHNUM<<endl;
	return -1;
      }
      path_list[path_no].id=id;
      path_list[path_no].type_seq=&key_str[8];
      path_list[path_no].count=0;
      path_prob_sum[path_no]=0;
    }
    path_prob_sum[path_no]+=cur_prob;
    path_list[path_no].count++;
    prob_sum[id]+=cur_prob;
  }
  int id_type=(id<item_head)? 0 : ((id<item_tail)? 3:6);
  for(int i=0;i<edge_pos[1][id];i++) {
    if(visited[adjacent_list[id][i].to]>0) continue;
    double update_prob=cur_prob*adjacent_list[id][i].weight;
    if(update_prob<threshold_prob) continue;
    path[depth]=(i<edge_pos[0][id])? path_type[id_type]: tag_type(id_type,adjacent_list[id][i].to);
    if((status=find_path(adjacent_list[id][i].to,update_prob,depth+1))<0) break;
  }
  visited[id]=0;
  return status;
}

void
graph::set_rec_list(RecInfo *rec_list, int rec_num) {
  heap *rec_rank=new heap(rec_num,prob_sum,node_num);
  int pos=item_head;
  for(;pos<item_tail;pos++) {
    if(prob_sum[pos]<0) continue;
    if(rec_rank->insert(pos)<0) break;
  }
  for(;pos<item_tail;pos++) {
    if(prob_sum[rec_rank->rank_list[0]]>=prob_sum[pos]) continue;
    rec_rank->insert_top(pos);    
  }
  rec_rank->sort();
  for(int i=0;i<rec_num;i++) {
    rec_list[i].id=rec_rank->rank_list[i];
    rec_list[i].prob=prob_sum[rec_list[i].id];
    rec_list[i].id-=item_head;
  }
  delete rec_rank;
}

void
graph::set_path_list(RecInfo *rec_list, int rec_num) {
  heap  **path_rank=new heap*[rec_num];
  for(int i=0;i<rec_num;i++) path_rank[i]=new heap(PATHSEQNUM,path_prob_sum,path_table->stored_num);
  for(int i=0;i<path_table->stored_num;i++) {
    int rank_pos;
    if((rank_pos=ranking[path_list[i].id-item_head])>=rec_num) continue;
    if(path_rank[rank_pos]->insert(i)>=0) continue;
    if(path_prob_sum[path_rank[rank_pos]->rank_list[0]]>=path_prob_sum[i]) continue;
    path_rank[rank_pos]->insert_top(i);    
  }
  for(int i=0;i<rec_num;i++) {
    path_rank[i]->sort();
    for(int j=0;j<PATHSEQNUM;j++) {
      if(j==path_rank[i]->last_pos) {
	rec_list[i].path_seq[j].prob=-1;
	break;
      }
      strcpy(rec_list[i].path_seq[j].type_seq,path_list[path_rank[i]->rank_list[j]].type_seq);
      rec_list[i].path_seq[j].prob=path_prob_sum[path_rank[i]->rank_list[j]];
      rec_list[i].path_seq[j].count=path_list[path_rank[i]->rank_list[j]].count;
    }
    delete path_rank[i];
  }
  delete[] path_rank;
  return;
}

int
graph::recommend(int user_id, int rec_num, RecInfo *rec_list, int *rated_movie, int rated_num) {
  path_table->hash_clear();
  strbuf->clear();  
  if(rated_movie==NULL) {
    for(int i=item_head;i<item_tail;i++) prob_sum[i]=0;
    for(int i=0;i<edge_pos[1][user_id];i++) prob_sum[adjacent_list[user_id][i].to]=-1;
  }
  else {
    for(int i=item_head;i<item_tail;i++) prob_sum[i]=-1;
    for(int i=0;i<rated_num;i++) prob_sum[item_head+rated_movie[i]]=0;
    if(rated_num<rec_num) rec_num=rated_num;
  }
  if(find_path(user_id,1,0)<0) return -1; //error occurred.
  set_rec_list(rec_list,rec_num);
  for(int i=item_head;i<item_tail;i++) ranking[i-item_head]=rec_num;
  for(int i=0;i<rec_num;i++) ranking[rec_list[i].id]=i;
  set_path_list(rec_list,rec_num);
  return rec_num;
}
