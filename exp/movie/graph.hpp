//
//  graph.hpp
//
#ifndef _INC_GRAPH
#define _INC_GRAPH

#include "strbuffer.hpp"
#include "hash.hpp"
#define DIFPATHNUM 1000000
#define KEY_LEN 20
#define PATHLENLIMIT 20
#define PATHSEQNUM 10

typedef struct {
  int to;
  double weight;
} Edge;

typedef struct {
  char type_seq[PATHLENLIMIT];
  double prob;
  int count;
} PathSeq;

typedef struct {
  int id;
  double prob;
  PathSeq path_seq[PATHSEQNUM];
} RecInfo;

typedef struct {
  int id;
  int count;
  char *type_seq;
} PathInfo;

class graph {
  int node_num;
  Edge *adjacent_list_area;
  int area_size;
  double threshold_prob;
  unsigned char *visited;
  double *prob_sum;
  double *path_prob_sum;  
  int item_head;
  int item_tail;
  PathInfo *path_list;
  strbuffer *strbuf;
  int *ranking;
  char path[PATHLENLIMIT];
  char key_buf[KEY_LEN];
  hashing *path_table;
  int find_path(int id, double cur_prob, int depth);
  void set_rec_list(RecInfo *rec_list, int rec_num);
  void set_path_list(RecInfo *rec_list, int rec_num);
  char tag_type(int id_type, int to_id);
  char *path_key(int id,int depth);
public:
  Edge **adjacent_list;
  int *edge_pos[2];
  graph(int in_node_num,int *edge_num, int user_num, int item_num, double in_threshold_prob);
  int recommend(int user_id, int rec_num, RecInfo *rec_list, int *rated_movie, int rated_num);
};

#endif    //INC_GRAPH
