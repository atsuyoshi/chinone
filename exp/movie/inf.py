def rec(n):  
    import pandas as pd
    import numpy as np
    from scipy import sparse
    import math
    from sklearn.model_selection import KFold
    import csv
    from cppRelationPath import RelationPath

    #データの取得
    df_tags = pd.read_csv('ml-latest-small/tags.csv')
    df_movies = pd.read_csv('ml-latest-small/movies.csv')
    df_movies = df_movies.drop('genres',axis=1)
    df_ratings = pd.read_csv('ml-latest-small/ratings.csv')

    df_tags_combined = df_tags.groupby('movieId').apply(lambda x: list(x['tag'])).reset_index().rename(columns={0:'tags'})
    dfm = pd.merge(df_movies, df_tags_combined, on = 'movieId', how = 'left')
    dfm['tags'] = dfm['tags'].apply(lambda x: x if isinstance(x,list) else [])
    all_tags = set()
    for this_movie_tags in dfm['tags']:
        all_tags = all_tags.union(this_movie_tags)
    all_tags = list(all_tags)
    tagg = pd.DataFrame(all_tags,columns = ['tag'])
    tagg['tagId'] = range(len(tagg))
    dff = pd.merge(df_tags,tagg)

    lu = 610
    lm = len(df_movies)
    lt = len(df_tags)
    lta = len(tagg)

    df_movies['movieId'] = range(lm)
    dfr = pd.merge(df_movies, df_ratings, on = 'movieId')
    dR = dfr.loc[:,['userId','movieId','rating']]
    dR = dR.sample(frac=1, random_state=0)

    ndcg_sum = np.zeros(lu)
    kf = KFold(n_splits=5,shuffle=True,random_state=24)

    dft = pd.merge(dff,df_movies)
    dT = dft.loc[:,['userId','movieId','tagId']]
    co = 0

    with open('ndcg10.csv','w') as f:
        writer = csv.writer(f, lineterminator='\n')

    for train, test in kf.split(dR):
        print(co)
        train_df = dR.iloc[train]
        train_df = train_df.reset_index(drop=True)
        test_df = dR.iloc[test]
        test_df = test_df.reset_index(drop=True)
        lrt = len(test_df)
   
        rp=RelationPath(train_df.values,dT.values,lu,lm,lta,0.00000001)

        rp.set_test_data(test_df.values)
        rp.create_rec_buffer(n)

        shape_z = (lu, lm)
        Z = sparse.lil_matrix(shape_z)
        for i in range(lrt):
            Z[test_df.at[i,'userId'] - 1, test_df.at[i,'movieId']]  = test_df.at[i,'rating']
        
        for u in range(lu):
            print('u: %d'%(u))
            rec_num=rp.recommend(u)

            for i in range(rec_num):
                user_id,prob=rp.get_rec_info(i)
                print(user_id,":",prob)
                for j in range(10):
                    type_seq,prob,count=rp.get_path_info(i,j)
                    if(prob<0):
                        break
                    print("[",type_seq,",",prob,",",count,"]",end='')
                print()
            #test_dataのtarget_userのratings
            z = np.zeros(lm)
            z = Z[u]
            z = z.todense()
            z = np.ravel(z)

            rate1 = np.zeros(n)
            rate2 = np.zeros(n)            
            
            for i in range(n):
                if(i < rec_num):
                    rate1[i] = z[rp.get_rec_id(i)]
                else:
                    rate1[i] = 0
                rate2[i] = np.sort(z)[::-1][i]
                print(rate1[i],rate2[i])
            
            dcg = 0
            idcg = 0
            
            for i in range(n):
                dcg += (2**rate1[i] - 1)/ math.log(i+2,2)
                idcg += (2**rate2[i] - 1)/ math.log(i+2,2)

            ndcg = dcg / idcg

            print('ndcg: '+ str(ndcg))
            ndcg_sum[u] += ndcg
        co += 1
    
    ndcg_sum /= 10

    for u in range(lu):
        with open('ndcg10.csv','a') as f:
            writer = csv.writer(f,lineterminator='\n')
            writer.writerow([ndcg_sum[u]])     
    
    
    return

if __name__ == '__main__':
    rec(10)
