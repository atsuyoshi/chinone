//
//  cppRelationPath_.hpp
//  ヘッダーファイル
//
#ifndef _INC_CPPRELATIONPATH
#define _INC_CPPRELATIONPATH

#include "graph.hpp"

namespace cppRelationPath {

class cppRelationPath {
  int node_num;
  int user_num;
  int movie_num;
  int *user_edge_num;
  int *movie_edge_num;
  int *tag_edge_num;
  graph *G;
  int *rated_movie_area;
  int **rated_movie;
  int *rated_num;
  void set_rating_weight(double *rating_data,int rating_num);
  void set_tag_weight(long *tag_data, int tag_num);
  void rating_weight_normalization();  
  void merge_tag_weight();
  void tag_weight_normalization();  
  void weight_normalization();
public:
  RecInfo *rec_list;
  int rec_num;
  cppRelationPath(double *rating_data, int rating_num, long *tag_data, int tag_num, int u_num, int m_num, int t_num, double threshold_prob);
  ~cppRelationPath();
  void set_test_data(double *rating_data, int rating_num);
  void create_rec_buffer(int in_rec_num);
  int recommend(int user_id);
};

}
#endif   
