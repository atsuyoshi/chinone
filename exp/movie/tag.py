import pandas as pd
import numpy as np
df_tags = pd.read_csv('ml-20m/tags.csv')
df_movies = pd.read_csv('ml-20m/movies.csv')
df_movies = df_movies.drop('genres',axis=1)
df_ratings = pd.read_csv('ml-20m/ratings.csv')

df_tags_combined = df_tags.groupby('movieId').apply(lambda x: list(x['tag'])).reset_index().rename(columns={0:'tags'})
dfm = pd.merge(df_movies, df_tags_combined, on = 'movieId', how = 'left')
dfm['tags'] = dfm['tags'].apply(lambda x: x if isinstance(x,list) else [])
all_tags = set()
for this_movie_tags in dfm['tags']:
    all_tags = all_tags.union(this_movie_tags)
all_tags = list(all_tags)
tagg = pd.DataFrame(all_tags,columns = ['tag'])
tagg['tagId'] = range(len(tagg))
dff = pd.merge(df_tags,tagg)
lm = len(df_movies)
df_movies['movieid'] = range(lm)
dfr = pd.merge(df_movies, df_ratings, on = 'movieId')

dR = dfr.loc[:,['userId','movieid','rating']]
#print(dR[dR['movieid'] == 293])
#print(dR.groupby('userId').count()['movieid']))
print(np.argmin(dR.groupby('userId').count()['movieid']))
dft = pd.merge(dff,df_movies)
dT = dft.loc[:,['userId','movieid','tagId']]
#print(np.max(dT.groupby('userId').count()['movieid']))
#print(np.argmax(dT.groupby('userId').count()['movieid']))