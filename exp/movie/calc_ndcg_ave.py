import pandas as pd
import numpy as np

df = pd.read_csv('./result/ndcg.csv',skip_blank_lines=False,header=None)
print(df)
print(df.mean())
df = pd.read_csv('./result/precision.csv',skip_blank_lines=False,header=None)
print(df)
print(df.mean())
df = pd.read_csv('./result/recall.csv',skip_blank_lines=False,header=None)
print(df)
print(df.mean())
df = pd.read_csv('./result/f1.csv',skip_blank_lines=False,header=None)
print(df)
print(df.mean())

